package com.yuriy.detection;

/**
 * Created with IntelliJ IDEA.
 * User: Yuriy Chernyshov
 * Date: 7/5/15
 * Time: 8:19 PM
 *
 */
public enum RoadSign {
    /**
     *
     */
    NO_LEFT_TURN(0),
    /**
     *
     */
    NO_RIGHT_TURN(1),
    /**
     *
     */
    NO_U_TURN(2),
    /**
     *
     */
    STOP(3),
    /**
     *
     */
    NO_PARKING(4),
    /**
     *
     */
    NO_STOPPING(5),
    /**
     *
     */
    SPEED_LIMIT_EU(6),
    /**
     *
     */
    SPEED_LIMIT_NA(7),
    /**
     *
     */
    UNKNOWN(-1);

    private final int id;

    RoadSign(final int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static RoadSign getSign(final int id) {
        int counter = 0;
        for (final RoadSign roadSign : values()) {
            if (counter++ == id) {
                return roadSign;
            }
        }
        return UNKNOWN;
    }
}
