/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection;

import com.yuriy.detection.DetectedListener;
import com.yuriy.detection.DetectionCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/9/14
 * Time: 10:32 PM
 */
public final class CompositeEllipseLikeDetector implements DetectedListener {

    private final List<DetectedListener> listeners = new ArrayList<>();

    public CompositeEllipseLikeDetector(final DetectedListener... listeners) {
        Collections.addAll(this.listeners, listeners);
    }

    @Override
    public final void detectShape(final int[] detectedRed,
                                  final int[] detectedRGB,
                                  final int detectedWidth,
                                  final int detectedHeight,
                                  final DetectionCallback callback) {

        for (final DetectedListener listener : listeners) {
            if (listener != null) {
                listener.detectShape(
                        detectedRed, detectedRGB, detectedWidth, detectedHeight, callback
                );
            }
        }
    }
}
