package com.yuriy.detection.graham_scan;

import com.yuriy.detection.matching_squares.Point;

import java.util.Arrays;
import java.util.Stack;

/**
 * Created by Chernyshov Yurii
 * At Intellij IDEA
 * On 02/01/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */

/**
 * http://algs4.cs.princeton.edu/99hull/GrahamScan.java.html
 * http://algs4.cs.princeton.edu/99hull/Point2D.java.html
 */
public final class GrahamScan {

    private final Stack<Point> hull = new Stack<>();

    public GrahamScan() {
        super();
    }

    /**
     * Computes the convex hull of the specified array of points.
     *
     * @param pts the array of points
     * @throws NullPointerException if {@code points} is {@code null} or if any
     *                              entry in {@code points[]} is {@code null}
     */
    public GrahamScan(final Point[] pts) {

        // defensive copy
        final int n = pts.length;
        final Point[] points = new Point[n];
        System.arraycopy(pts, 0, points, 0, n);

        // pre-process so that points[0] has lowest y-coordinate; break ties by x-coordinate
        // points[0] is an extreme point of the convex hull
        // (alternatively, could do easily in linear time)
        //Arrays.sort(points);

        // sort by polar angle with respect to base point points[0],
        // breaking ties by distance to points[0]
        Arrays.sort(points, 1, n, points[0].polarOrder());

        // p[0] is first extreme point
        hull.push(points[0]);

        // find index k1 of first point not equal to points[0]
        int k1;
        for (k1 = 1; k1 < n; ++k1) {
            if (!points[0].equals(points[k1])) {
                break;
            }
        }

        // all points equal
        if (k1 == n) {
            return;
        }

        // find index k2 of first point not collinear with points[0] and points[k1]
        int k2;
        for (k2 = k1 + 1; k2 < n; ++k2) {
            if (Point.ccw(points[0], points[k1], points[k2]) != 0) {
                break;
            }
        }
        // points[k2-1] is second extreme point
        hull.push(points[k2 - 1]);

        // Graham scan; note that points[n-1] is extreme point different from points[0]
        Point top;
        for (int i = k2; i < n; ++i) {
            top = hull.pop();
            while (!hull.empty() && Point.ccw(hull.peek(), top, points[i]) <= 0) {
                top = hull.pop();
            }
            hull.push(top);
            hull.push(points[i]);
        }

        //assert isConvex();
    }

    /**
     * Returns the extreme points on the convex hull in counterclockwise order.
     *
     * @return the extreme points on the convex hull in counterclockwise order
     */
    public Point[] hull() {
        final Point[] points = new Point[hull.size()];
        hull.toArray(points);
        return points;
    }

    // check that boundary of hull is strictly convex
    private boolean isConvex() {
        final int n = hull.size();
        if (n <= 2) {
            return true;
        }

        final Point[] points = new Point[n];
        int k = 0;
        final Point[] hull = hull();
        for (final Point point : hull) {
            points[k++] = point;
        }

        for (int i = 0; i < n; ++i) {
            if (Point.ccw(points[i], points[(i + 1) % n], points[(i + 2) % n]) <= 0) {
                return false;
            }
        }
        return true;
    }
}
