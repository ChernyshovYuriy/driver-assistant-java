/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection;

/**
 * Created at Intellij IDEA
 * Author Yuri Chernyshov
 * on 01.06.14
 * at 8:49
 */
public interface ImageDetectingCallback {

    /**
     * This function is call when a {@link DetectedShape} has been detected.
     *
     * @param detectedShape detected {@link DetectedShape}.
     */
    void onDetect(final DetectedShape detectedShape);
}