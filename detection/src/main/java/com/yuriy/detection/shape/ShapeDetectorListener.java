/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.shape;

import com.yuriy.detection.matching_squares.Path;
import com.yuriy.detection.ImageDetectingCallback;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/5/14
 * Time: 8:07 AM
 */

/**
 * Interface to track shape detector.
 */
public interface ShapeDetectorListener {

    /**
     * Call this method when a {@link com.yuriy.detection.matching_squares.Path} is need to be detected.
     *
     * @param path                   The path of the points.
     * @param dataRed                The data representing covered {@link Path} in Red Color.
     * @param dataRGB                The data representing covered {@link Path} in RGB Color space.
     * @param imageDetectingCallback The callback function to call on different phases of image detection.
     */
    void detectShape(final Path path,
                     final int[] dataRed,
                     final int[] dataRGB,
                     final ImageDetectingCallback imageDetectingCallback);
}