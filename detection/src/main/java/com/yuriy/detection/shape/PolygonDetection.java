/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.shape;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/4/14
 * Time: 8:57 PM
 */

import com.yuriy.detection.DetectedShape;
import com.yuriy.detection.ImageDetectingCallback;
import com.yuriy.detection.matching_squares.Path;
import com.yuriy.detection.matching_squares.Point;
import com.yuriy.detection.utils.PointsFactory;

/**
 *
 */
public final class PolygonDetection extends BaseDetection {

    Point[] points;

    public PolygonDetection() {
        super();
    }

    @Override
    public final void detectShape(final Path path,
                                  final int[] dataRed,
                                  final int[] dataRGB,
                                  final ImageDetectingCallback imageProcessingCallback) {

        init();

        Point pointA;
        Point pointB;
        points = PointsFactory.createPoints(
                path.getOriginX(), path.getOriginY(), path.getDirections()
        );
        int pointsLength = points.length;
        double d;
        int rise;
        int run;
        int rectSidesNumber = 1;
        int octagonSidesNumber = 1;
        int prevAngle = Integer.MAX_VALUE;
        int angle;
        int angleDelta;
        final int threshold = path.getLongestSide() * 10 / 100;
//        System.out.println("New Shape:" + pointsLength);
        for (int i = 1; i <= pointsLength; ++i) {

//            System.out.println("I:" + i + " L:" + (points.length));
            if (i == pointsLength) {
                pointA = points[0];
                pointB = points[pointsLength - 1];
//                System.out.println(" p1:" + 0 + " p2:" + (pointsLength - 1));
            } else {
                pointA = points[i - 1];
                pointB = points[i];
//                System.out.println(" p1:" + (i - 1) + " p2:" + i);
            }

            rise = pointB.y - pointA.y;
            run = pointB.x - pointA.x;
            d = Math.sqrt((run * run) + (rise * rise));
            if (d <= threshold) {
                continue;
            }
            angle = angle(pointB, pointA);
            angleDelta = Math.abs(angle - prevAngle);
//            System.out.println(" ang:" + angle + " \tprevAng:" + prevAngle + " \t" + d);
//            System.out.println(" ang delta:" + angleDelta);
            if (prevAngle != Integer.MAX_VALUE) {
                if (angleDelta >= 85 && angleDelta <= 95) {
//                  System.out.println("  ++");
                    rectSidesNumber++;
                }
                if (angleDelta >= 35 && angleDelta <= 55) {
                    octagonSidesNumber++;
                }
            }
            prevAngle = angle;
        }

//        System.out.println("Rect    Sides number:" + rectSidesNumber);
//        System.out.println("Octagon Sides number:" + octagonSidesNumber);

        if (rectSidesNumber == 4) {
            imageProcessingCallback.onDetect(
                    new DetectedShape(DetectedShape.SHAPE.RECTANGLE, path, dataRed, dataRGB)
            );
        }
        if (octagonSidesNumber == 8) {
            imageProcessingCallback.onDetect(
                    new DetectedShape(DetectedShape.SHAPE.OCTAGON, path, dataRed, dataRGB)
            );
        }
    }

    /**
     * Detect angle between two points.
     *
     * @param pointA Point A.
     * @param pointB Point B.
     * @return Angle between two points in degrees.
     */
    static int angle(final Point pointA, final Point pointB) {
        final double deltaY = pointA.y - pointB.y;
        final double deltaX = pointB.x - pointA.x;
        return (int) Math.toDegrees(Math.abs(Math.atan2(deltaY, deltaX)));
    }
}
