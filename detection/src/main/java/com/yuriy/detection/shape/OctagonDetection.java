/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.shape;

import com.yuriy.detection.ImageDetectingCallback;
import com.yuriy.detection.DetectedShape;
import com.yuriy.detection.matching_squares.Direction;
import com.yuriy.detection.matching_squares.Path;

/**
 * Created with IntelliJ IDEA.
 * User: Yuriy Chernyshov
 * Date: 7/1/15
 * Time: 10:02 AM
 */

/**
 *
 */
public final class OctagonDetection extends BaseDetection {

    public OctagonDetection() {
        super();
    }

    @Override
    public final void detectShape(final Path path,
                                  final int[] dataRed,
                                  final int[] dataRGB,
                                  final ImageDetectingCallback imageDetectingCallback) {
        //System.out.println("Octagon:" + path.getTerminalX() + " " + path.getTerminalY());
        int prevX = -1;
        int prevY = -1;
        int missCount = 0;
        for (final Direction direction : path.getDirections()) {
            //System.out.println(" : " + prevX + " " + prevY + " - " + direction.planeX + " " + direction.planeY);
            if (prevX == -1 && prevY == -1) {
                prevX = direction.screenX;
                prevY = direction.screenY;
                continue;
            }
            if (direction.screenX != prevX || direction.screenY != prevY) {
                if (missCount++ < 2) {
                    continue;
                }
                prevX = direction.screenX;
                prevY = direction.screenY;

                //System.out.println("Direction changed");
            }

            missCount = 0;
        }

        imageDetectingCallback.onDetect(
                new DetectedShape(DetectedShape.SHAPE.OCTAGON, path, dataRed, dataRGB)
        );
    }
}
