/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.shape;

import com.yuriy.detection.DetectedShape;
import com.yuriy.detection.ImageDetectingCallback;
import com.yuriy.detection.matching_squares.Direction;
import com.yuriy.detection.matching_squares.Path;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/5/14
 * Time: 8:42 AM
 */

/**
 * This class defines whether the point of the shape is belongs to triangle
 */
public final class TriangleDetection extends BaseDetection {

    /**
     * Percentage number to deviate around square of rectangle when calculate a sum of the three triangles
     */
    private static final int DELTA = 10;

    public TriangleDetection() {
        super();
    }

    @Override
    public final void detectShape(final Path path,
                                  final int[] dataRed,
                                  final int[] dataRGB,
                                  final ImageDetectingCallback imageDetectingCallback) {

        init();

        Direction direction;

        int lastX = path.getOriginX() - path.getMinX();
        int lastY = path.getShapeHeight();

        int x1 = path.getTopPoint()[0] - path.getMinX();
        int y1 = path.getTopPoint()[1] - (-path.getOriginY());
        y1 = -y1 + path.getShapeHeight();

        int x2 = path.getLeftPoint()[0] - path.getMinX();
        int y2 = path.getLeftPoint()[1] - (-path.getOriginY());
        y2 = -y2 + path.getShapeHeight();

        int x3 = path.getRightPoint()[0] - path.getMinX();
        int y3 = path.getRightPoint()[1] - (-path.getOriginY());
        y3 = -y3 + path.getShapeHeight();

        int square = calculateSquare(x1, y1, x2, y2, x3, y3);

        final Direction[] directions = path.getDirections();
        for (int i = 0; i < directions.length; i++) {

            direction = directions[i];

            lastX += direction.screenX;
            lastY -= direction.screenY;

            //double Bx, By, Cx, Cy, Px, Py;
            //double m, l;

            if (i % BASE_PROCESS_PERIOD == 0) {

                incrementPointsNumber();

                int s1 = calculateSquare(x2, y2, lastX, lastY, x1, y1);
                int s2 = calculateSquare(x2, y2, x3, y3, lastX, lastY);
                int s3 = calculateSquare(lastX, lastY, x3, y3, x1, y1);

                int s = s1 + s2 + s3;
                final int hundredPercent = 100;
                if (s > (square + (square * DELTA / hundredPercent))
                        || s < (square - (square * DELTA / hundredPercent))) {
                    incrementFailCounter();
                }

                /*Bx = bX - aX;
                By = bY - aY;
                Cx = cX - aX;
                Cy = cY - aY;
                Px = (lastX - path.getMinX()) - aX;
                Py = (lastY - path.getMinY()) - aY;

                m = (double)(Px * By - Bx * Py) / (double)(Cx * By - Bx * Cy);
                AppLogger.printMessage("M:" + m);

                if (m >= 0 && m <= 1) {
                    l = (Px - m * Cx) / Bx;
                    AppLogger.printMessage("   L:" + l);
                    if (l >= 0 && m + l <= 1) {
                        // TRUE
                        //AppLogger.printMessage("TRUE");
                    }
                }*/
            }
        }

        if (getResult()) {
            imageDetectingCallback.onDetect(
                    new DetectedShape(DetectedShape.SHAPE.TRIANGLE, path, dataRed, dataRGB)
            );
        }
    }

    /**
     * This method calculates a square of the triangle
     *
     * @param aX X coordinate of the A point
     * @param aY Y coordinate of the A point
     * @param bX X coordinate of the B point
     * @param bY Y coordinate of the B point
     * @param cX X coordinate of the C point
     * @param cY Y coordinate of the C point
     *
     * @return calculated square
     */
    private int calculateSquare(final int aX, final int aY, final int bX, final int bY, final int cX, final int cY) {
        int s = (bX * cY - cX * bY - aX * cY + cX * aY + aX * bY - bX * aY);
        return s < 0 ? -s : s;
    }
}