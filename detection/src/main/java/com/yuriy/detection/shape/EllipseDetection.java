/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.shape;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/4/14
 * Time: 8:57 PM
 */

import com.yuriy.detection.DetectedShape;
import com.yuriy.detection.ImageDetectingCallback;
import com.yuriy.detection.matching_squares.Direction;
import com.yuriy.detection.matching_squares.Path;

/**
 * This class defines whether the point of the shape is belongs to ellipse by using <b>Canonical form</b>
 * http://en.wikipedia.org/wiki/Ellipse#Canonical_form
 */
public final class EllipseDetection extends BaseDetection {

    /**
     * Delta value of the deviation near the 1 (see <b>Canonical form</b>)
     */
    private static final double DELTA = 0.2;

    /**
     * Radius of the ellipse in X axis
     */
    private int a;

    /**
     * Radius of the ellipse in Y axis
     */
    private int b;

    public EllipseDetection() {
        super();
    }

    @Override
    public final void detectShape(final Path path,
                                  final int[] dataRed,
                                  final int[] dataRGB,
                                  final ImageDetectingCallback imageProcessingCallback) {

        Direction direction;
        int lastX = path.getOriginX() - path.getMinX();
        int lastY = path.getShapeHeight();

        init();

        a = path.getShapeWidth() >> 1;
        b = path.getShapeHeight() >> 1;

        processPoint(lastX, lastY);

        final Direction[] directions = path.getDirections();
        for (int i = 0; i < directions.length; i++) {

            direction = directions[i];

            lastX += direction.screenX;
            lastY -= direction.screenY;

            if (i % BASE_PROCESS_PERIOD == 0) {
                processPoint(lastX, lastY);
            }
        }

        if (getResult()) {
            imageProcessingCallback.onDetect(
                    new DetectedShape(DetectedShape.SHAPE.ELLIPSE, path, dataRed, dataRGB)
            );
        }
    }

    /**
     * Process a single point of the shape
     * @param x X coordinate of the point
     * @param y Y coordinate of the point
     */
    private void processPoint(final int x, final int y) {
        int xTmp = x - a;
        int yTmp = y - b;
        double result = ((double) (xTmp * xTmp) / (double) (a * a)) + ((double) (yTmp * yTmp) / (double) (b * b));

        incrementPointsNumber();
        if (result < 1 - DELTA || result > 1 + DELTA) {
            incrementFailCounter();
        }
    }
}
