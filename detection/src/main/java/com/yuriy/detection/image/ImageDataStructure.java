/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.image;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/14/13
 * Time: 7:44 PM
 */

/**
 * This class represents a data structure that holds objects of {@link com.yuriy.detection.image.ImageVertex} type
 */
public final class ImageDataStructure {

    /**
     * Data structure holder.
     */
    private ImageVertex[] vertexes;

    /**
     * Flag that indicates whether data structure has been initialized.
     */
    private boolean isInit = false;

    /**
     * Constructor.
     */
    public ImageDataStructure() {
        super();
    }

    /**
     * Initialize data structure.
     *
     * @param pixelsNumber Number of the pixels in the single frame (image).
     */
    public final void init(final int pixelsNumber) {
        vertexes = new ImageVertex[pixelsNumber];
        int counter = 0;
        for (ImageVertex imageVertex : vertexes) {
            vertexes[counter++] = ImageVertex.createDefaultInstance();
        }

        isInit = true;
    }

    /**
     * Reset data structure.
     */
    public final void reset() {
        int counter = 0;
        for (ImageVertex imageVertex : vertexes) {
            vertexes[counter++].reset();
        }
    }

    /**
     * Factory method to create instance of the {@link com.yuriy.detection.image.ImageDataStructure}.
     *
     * @return Instance of the {@link com.yuriy.detection.image.ImageDataStructure}.
     */
    public static ImageDataStructure createInstance() {
        return new ImageDataStructure();
    }

    /**
     * @return True is the data structure has been initialized, False - otherwise.
     */
    public final boolean isInit() {
        return isInit;
    }

    /**
     * Add a vertex to the com.yuriy.detection.image
     *
     * @param position    Position of the element.
     * @param imageVertex Object of the {@link com.yuriy.detection.image.ImageVertex} type.
     */
    public final void addVertex(final int position, final ImageVertex imageVertex) {
        vertexes[position] = imageVertex;
    }

    /**
     * Get a vertex at specified position.
     *
     * @param position position of the vertex in the List.
     *
     * @return object of the {@link com.yuriy.detection.image.ImageVertex} type.
     */
    public final ImageVertex getVertexAt(final int position) {
        if (position > vertexes.length - 1) {
            return ImageVertex.createDefaultInstance();
        }
        return vertexes[position];
    }

    /**
     * @return a size of the vertexes in the com.yuriy.detection.image.
     */
    public final int getVertexesSize() {
        return vertexes.length;
    }

    /**
     * Returns NHS array of the Red color.
     *
     * @return Array of the NHS pixels.
     */
    public final int[] getNhsRedAsArray() {
        final int[] result = new int[vertexes.length];
        for (int i = 0; i < vertexes.length; i++) {
            result[i] = vertexes[i].getNhsRed();
        }
        return result;
    }

    /**
     * Returns array of the Luminance for each pixel.
     *
     * @return The array of the Luminance.
     */
    public final int[] getLuminanceAsArray() {
        final int[] result = new int[vertexes.length];
        for (int i = 0; i < vertexes.length; i++) {
            result[i] = vertexes[i].getLuminance();
        }
        return result;
    }

    public final int[] getWhiteAsArray() {
        final int[] result = new int[vertexes.length];
        for (int i = 0; i < vertexes.length; i++) {
            result[i] = vertexes[i].getWhite();
        }
        return result;
    }
}
