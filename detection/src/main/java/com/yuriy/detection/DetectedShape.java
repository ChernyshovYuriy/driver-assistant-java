/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection;

import com.yuriy.detection.matching_squares.Path;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/7/14
 * Time: 8:59 PM
 */
public final class DetectedShape {

    public enum SHAPE {
        UNKNOWN,
        TRIANGLE,
        ELLIPSE,
        OCTAGON,
        RECTANGLE,
    }

    private final Path path;

    private final SHAPE shape;

    private final int[] dataRed;

    private final int[] dataRGB;

    public DetectedShape(final SHAPE shape,
                         final Path path,
                         final int[] dataRed,
                         final int[] dataRGB) {
        super();
        this.shape = shape;
        this.path = path;
        this.dataRed = dataRed;
        this.dataRGB = dataRGB;
    }

    public SHAPE getShape() {
        return shape;
    }

    public Path getPath() {
        return path;
    }

    public int[] getDataRed() {
        return dataRed;
    }

    public int[] getDataRGB() {
        return dataRGB;
    }
}