package com.yuriy.detection.utils;

import com.yuriy.detection.matching_squares.Direction;
import com.yuriy.detection.matching_squares.Point;

/**
 * Created by Chernyshov Yurii
 * At Intellij IDEA
 * On 22/12/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class PointsFactory {

    private PointsFactory() {
        super();
    }

    public static Point[] createPoints(final int x, final int y, final Direction[] directions) {
        final Point[] points = new Point[directions.length];
        Point point;
        int counter = 0;
        for (final Direction direction : directions) {
            point = new Point(x, -y, direction);
            points[counter++] = point;
        }
        return points;
    }
}
