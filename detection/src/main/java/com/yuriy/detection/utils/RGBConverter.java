/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.utils;

import com.yuriy.detection.ihls_nhs.IHLS_NHS;
import com.yuriy.detection.image.ImageDataStructure;
import com.yuriy.detection.image.ImageVertex;

/**
 * Created at Intellij IDEA
 * Author Yuri Chernyshov
 * on 25.05.14
 * at 17:13
 *
 * {@link RGBConverter} is a class that provides various functionality to convert to different color space,
 * such as from RGB color space into NHS color space, to gray scaled image, etc ...
 */
public final class RGBConverter {

    private static final int WHITE_THRESHOLD = 130;

    /**
     * Default constructor.
     */
    private RGBConverter() {
        super();
    }

    /**
     * Convert RGB data into NHS data.
     * Use provided data structure in order to reuse it over processing loop.
     *
     * @param data               RGB data as bytes array.
     * @param imageDataStructure Instance of the {@link com.yuriy.detection.image.ImageDataStructure}.
     */
    public static void toNHS(final int[] data, final ImageDataStructure imageDataStructure) {
        int sourceImageR;
        int sourceImageG;
        int sourceImageB;
        int saturation;
        int hue;
        int luminance = 0;
        int nhsBlue = 0;
        int nhsRed;
        int counter = 0;
        int x = 0;
        int y = 0;
        ImageVertex imageVertex;

        final IHLS_NHS ihls_nhs = new IHLS_NHS();
        for (final int aData : data) {

            sourceImageR = (aData >> 16) & 0xff;
            sourceImageG = (aData >> 8) & 0xff;
            sourceImageB = (aData) & 0xff;

            hue = (int) ihls_nhs.retrieveNormalisedHue(sourceImageR, sourceImageG, sourceImageB);
            saturation = ihls_nhs.retrieveSaturation(sourceImageR, sourceImageG, sourceImageB);
            //luminance = (int) ihls_nhs.retrieveLuminance(sourceImageR, sourceImageG, sourceImageB);

            // Red
            nhsRed = ihls_nhs.getNHSRed(hue, saturation);

            imageVertex = imageDataStructure.getVertexAt(counter++);
            imageVertex.setxPosition(x);
            imageVertex.setyPosition(y);
            imageVertex.setRedColor(sourceImageR);
            imageVertex.setGreenColor(sourceImageG);
            imageVertex.setBlueColor(sourceImageB);
            imageVertex.setHue(hue);
            imageVertex.setSaturation(saturation);
            imageVertex.setLuminance(luminance);
            imageVertex.setNhsBlue(nhsBlue);
            imageVertex.setNhsRed(nhsRed);
        }
    }

    /**
     *
     * @param data
     * @param imageDataStructure
     */
    public static void toLuminance(final int[] data, final ImageDataStructure imageDataStructure) {
        int sourceImageR;
        int sourceImageG;
        int sourceImageB;
        int luminance;
        int counter = 0;
        int x = 0;
        int y = 0;
        ImageVertex imageVertex;

        final IHLS_NHS ihls_nhs = new IHLS_NHS();
        for (final int aData : data) {

            sourceImageR = (aData >> 16) & 0xff;
            sourceImageG = (aData >> 8) & 0xff;
            sourceImageB = (aData) & 0xff;

            luminance = (int) ihls_nhs.retrieveLuminance(sourceImageR, sourceImageG, sourceImageB);

            imageVertex = imageDataStructure.getVertexAt(counter++);
            imageVertex.setxPosition(x);
            imageVertex.setyPosition(y);
            imageVertex.setRedColor(sourceImageR);
            imageVertex.setGreenColor(sourceImageG);
            imageVertex.setBlueColor(sourceImageB);
            imageVertex.setLuminance(luminance);
        }
    }

    /**
     *
     * @param data
     * @param imageDataStructure
     */
    public static void toBinaryWhite(final int[] data, final ImageDataStructure imageDataStructure) {
        int sourceImageR;
        int sourceImageG;
        int sourceImageB;
        int counter = 0;
        int x = 0;
        int y = 0;
        int white;
        ImageVertex imageVertex;

        for (final int aData : data) {

            sourceImageR = (aData >> 16) & 0xff;
            sourceImageG = (aData >> 8) & 0xff;
            sourceImageB = (aData) & 0xff;

            white = (sourceImageR >= WHITE_THRESHOLD
                    && sourceImageG >= WHITE_THRESHOLD
                    && sourceImageB >= WHITE_THRESHOLD)
                    ? 1 : 0;

            imageVertex = imageDataStructure.getVertexAt(counter++);
            imageVertex.setxPosition(x);
            imageVertex.setyPosition(y);
            imageVertex.setRedColor(sourceImageR);
            imageVertex.setGreenColor(sourceImageG);
            imageVertex.setBlueColor(sourceImageB);
            imageVertex.setWhite(white);
        }
    }

    /**
     *
     * @param data
     */
    public static void toGrayScale(final int[] data) {
        int sourceImageR;
        int sourceImageG;
        int sourceImageB;
        int counter = 0;

        for (final int aData : data) {

            sourceImageR = (aData >> 16) & 0xff;
            sourceImageG = (aData >> 8) & 0xff;
            sourceImageB = (aData) & 0xff;

            int grayLevel = (sourceImageR + sourceImageG + sourceImageB) / 3;
            int gray = (grayLevel << 16) + (grayLevel << 8) + grayLevel;
            data[counter++] = gray;
//            data[counter++] = (int) (0.2126 * sourceImageR + 0.7152 * sourceImageG + 0.0722 * sourceImageB);
        }
    }
}
