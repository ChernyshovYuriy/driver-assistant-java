/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.utils;

/*
 * If someone wants to directly retrieve hue, saturation or luminance based on RGB values.
 *
 */
public final class DefineConstants {

    /**
     * Color
     */
    public static final int R_HUE_MAX = 15;
    public static final int R_HUE_MIN = 240;
    public static final int R_SAT_MIN = 25;
    public static final int B_HUE_MAX = 163;
    public static final int B_HUE_MIN = 134;
    public static final int B_SAT_MIN = 39;
}
