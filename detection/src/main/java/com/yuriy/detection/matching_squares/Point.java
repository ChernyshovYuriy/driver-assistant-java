package com.yuriy.detection.matching_squares;

import java.lang.ref.WeakReference;
import java.util.Comparator;

/**
 * Created by Chernyshov Yurii
 * At Intellij IDEA
 * On 22/12/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class Point implements Comparable<Point> {

    public int x;
    public int y;
    public Direction direction;

    public Point(final int x, final int y, final Direction direction) {
        super();
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    /**
     * Compares two points by polar angle (between 0 and 2&pi;) with respect to this point.
     *
     * @return the comparator
     */
    public Comparator<Point> polarOrder() {
        return new Point.PolarOrder(this);
    }

    @Override
    public int compareTo(final Point point2) {
        return 0;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    /**
     * Returns true if a→b→c is a counterclockwise turn.
     *
     * @param a first point
     * @param b second point
     * @param c third point
     * @return { -1, 0, +1 } if a→b→c is a { clockwise, collinear; counterclockwise } turn.
     */
    public static int ccw(final Point a, final Point b, final Point c) {
        double area2 = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
        if (area2 < 0) return -1;
        else if (area2 > 0) return 1;
        else return 0;
    }

    // compare other points relative to polar angle (between 0 and 2pi) they make with this Point
    private static class PolarOrder implements Comparator<Point> {

        private final WeakReference<Point> reference;

        private PolarOrder(final Point reference) {
            super();
            this.reference = new WeakReference<>(reference);
        }

        public int compare(final Point point1, final Point point2) {
            final Point point = this.reference.get();
            if (point == null) {
                return 0;
            }

            double dx1 = point1.x - point.x;
            double dy1 = point1.y - point.y;
            double dx2 = point2.x - point.x;
            double dy2 = point2.y - point.y;

            if (dy1 >= 0 && dy2 < 0) return -1;    // point1 above; point2 below
            else if (dy2 >= 0 && dy1 < 0) return 1;    // point1 below; point2 above
            else if (dy1 == 0 && dy2 == 0) {            // 3-collinear and horizontal
                if (dx1 >= 0 && dx2 < 0) return -1;
                else if (dx2 >= 0 && dx1 < 0) return 1;
                else return 0;
            } else return -ccw(point, point1, point2);     // both above or below

            // Note: ccw() recomputes dx1, dy1, dx2, and dy2
        }
    }
}
