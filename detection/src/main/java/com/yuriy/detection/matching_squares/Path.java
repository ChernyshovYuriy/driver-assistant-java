/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection.matching_squares;

import java.util.Arrays;

/**
 * Combines a sequence of directions into a path that is rooted at some point in
 * the plane. No restrictions are placed on paths; they may be zero length,
 * open/closed, self-intersecting. Path objects are immutable.
 *
 * @author Tom Gibara
 */

public final class Path {

    private Direction[] directions;

    private final int originX;

    private final int originY;

    private int shapeWidth;

    private int minX = 0;

    private int maxX = 0;

    private int minY = 0;

    private int maxY = 0;

    private final int[] topPoint = new int[2];

    private final int[] bottomPoint = new int[2];

    private final int[] leftPoint = new int[2];

    private final int[] rightPoint = new int[2];

    /**
     * Constructs a path which starts at the specified point in the plane. The
     * array may be zero length.
     *
     * @param startX     the x coordinate of the path's origin in the plane
     * @param startY     the y coordinate of the path's origin in the plane
     * @param directions an array of directions, never null
     */
    public Path(final int startX, final int startY, final Direction[] directions) {
        super();
        this.originX = startX;
        this.originY = startY;
        this.directions = directions;
    }

    public Path(final Path path) {
        this(path.originX, path.originY, path.getDirections());
        shapeWidth = path.shapeWidth;
        minX = path.minX;
        maxX = path.maxX;
        minY = path.minY;
        maxY = path.maxY;
        setTopPoint(path.topPoint);
        setBottomPoint(path.bottomPoint);
        setLeftPoint(path.leftPoint);
        setRightPoint(path.rightPoint);
    }

    public int[] getTopPoint() {
        return topPoint;
    }

    public void setTopPoint(final int[] topPoint) {
        this.topPoint[0] = topPoint[0];
        this.topPoint[1] = topPoint[1];
    }

    public int[] getBottomPoint() {
        return bottomPoint;
    }

    public void setBottomPoint(final int[] bottomPoint) {
        this.bottomPoint[0] = bottomPoint[0];
        this.bottomPoint[1] = bottomPoint[1];
    }

    public int[] getLeftPoint() {
        return leftPoint;
    }

    public void setLeftPoint(final int[] leftPoint) {
        this.leftPoint[0] = leftPoint[0];
        this.leftPoint[1] = leftPoint[1];
    }

    public int[] getRightPoint() {
        return rightPoint;
    }

    public void setRightPoint(final int[] rightPoint) {
        this.rightPoint[0] = rightPoint[0];
        this.rightPoint[1] = rightPoint[1];
    }

    /**
     * @return the x coordinate in the plane at which the path begins
     */

    public int getOriginX() {
        return originX;
    }

    /**
     * @return the y coordinate in the plane at which the path begins
     */

    public int getOriginY() {
        return originY;
    }

    public Direction[] getDirections() {
        return Arrays.copyOf(directions, directions.length);
    }

    /**
     * @return a shapeWidth of the shape
     */
    public int getShapeWidth() {
        return shapeWidth;
    }

    /**
     * @return a shapeHeight of the shape
     */
    public int getShapeHeight() {
        return (maxY - minY);
    }

    public int getLongestSide() {
        return getShapeWidth() >= getShapeHeight() ? getShapeWidth() : getShapeHeight();
    }

    public int getMinX() {
        return minX;
    }

    public void setMinX(final int minX) {
        this.minX = minX;
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(final int maxX) {
        this.maxX = maxX;
    }

    public int getMinY() {
        return minY;
    }

    public void setMinY(final int minY) {
        this.minY = minY;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(final int maxY) {
        this.maxY = maxY;
    }

    /**
     * Set a shapeWidth of the shape
     *
     * @param shapeWidth
     */
    public void setShapeWidth(final int shapeWidth) {
        this.shapeWidth = shapeWidth;
    }

    /**
     * Two paths are equal if they have the same origin and the same directions.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Path))
            return false;
        final Path that = (Path) obj;

        return this.originX == that.originX
                && this.originY == that.originY
                && Arrays.equals(this.directions, that.directions);
    }

    @Override
    public int hashCode() {
        return originX ^ 7 * originY ^ Arrays.hashCode(directions);
    }

    @Override
    public String toString() {
        return "X: " + originX + ", Y: " + originY + " " + Arrays.toString(directions);
    }
}
