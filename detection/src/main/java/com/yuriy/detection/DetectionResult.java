package com.yuriy.detection;

/**
 * Created with IntelliJ IDEA.
 * User: Yuriy Chernyshov
 * Date: 6/9/16
 * Time: 1:18 PM
 */
public final class DetectionResult {

    private final int[] pixelsArray;
    private final int detectedWidth;
    private final int detectedHeight;

    public DetectionResult(final int[] pixelsArray, final int detectedWidth, final int detectedHeight) {
        super();
        this.pixelsArray = pixelsArray;
        this.detectedWidth = detectedWidth;
        this.detectedHeight = detectedHeight;
    }

    public int[] getPixelsArray() {
        return pixelsArray;
    }

    public int getDetectedWidth() {
        return detectedWidth;
    }

    public int getDetectedHeight() {
        return detectedHeight;
    }
}
