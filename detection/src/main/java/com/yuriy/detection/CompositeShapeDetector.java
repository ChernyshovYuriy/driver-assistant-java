/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.detection;

import com.yuriy.detection.ImageDetectingCallback;
import com.yuriy.detection.matching_squares.Path;
import com.yuriy.detection.shape.ShapeDetectorListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/5/14
 * Time: 8:19 AM
 */
public final class CompositeShapeDetector implements ShapeDetectorListener {

    private final List<ShapeDetectorListener> shapeDetectorListeners = new ArrayList<>();

    public CompositeShapeDetector(final ShapeDetectorListener... listeners) {
        Collections.addAll(shapeDetectorListeners, listeners);
    }

    @Override
    public final void detectShape(final Path path,
                                  final int[] dataRed,
                                  final int[] dataRGB,
                                  final ImageDetectingCallback imageDetectingCallback) {
        for (final ShapeDetectorListener listener : shapeDetectorListeners) {
            listener.detectShape(path, dataRed, dataRGB, imageDetectingCallback);
        }
    }
}
