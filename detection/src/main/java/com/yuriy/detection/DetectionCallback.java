package com.yuriy.detection;

/**
 * Created with IntelliJ IDEA.
 * User: Yuriy Chernyshov
 * Date: 7/5/15
 * Time: 1:18 PM
 */

/**
 * {@link DetectionCallback} is an interface to provide callback when detection is complete.
 */
public interface DetectionCallback {

    /**
     * On complete event.
     *
     * @param roadSign        Detected road sign type.
     * @param detectionResult Result of the detection.
     */
    void onComplete(final RoadSign roadSign, final DetectionResult detectionResult);
}
