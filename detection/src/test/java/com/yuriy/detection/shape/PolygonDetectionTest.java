package com.yuriy.detection.shape;

import com.yuriy.detection.matching_squares.Point;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Chernyshov Yurii
 * At Intellij IDEA
 * On 08/01/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class PolygonDetectionTest {

    public PolygonDetectionTest() {
        super();
    }

    @Test
    public void numbersSignChange() throws Exception {
        int a = 0;
        int b = 0;
        Assert.assertEquals(false, isSignChanged(a, b));

        a = 1;
        b = 1;
        Assert.assertEquals(false, isSignChanged(a, b));

        a = -1;
        b = -1;
        Assert.assertEquals(false, isSignChanged(a, b));

        a = -1;
        b = 1;
        Assert.assertEquals(true, isSignChanged(a, b));

        a = 1;
        b = -1;
        Assert.assertEquals(true, isSignChanged(a, b));

        a = -1;
        b = 0;
        Assert.assertEquals(true, isSignChanged(a, b));

        a = 0;
        b = 1;
        Assert.assertEquals(true, isSignChanged(a, b));

        a = 0;
        b = -1;
        Assert.assertEquals(true, isSignChanged(a, b));
    }

    @Test
    public void angle() throws Exception {
        Point pointA = new Point(527, 36, null);
        Point pointB = new Point(434, 35, null);

        Assert.assertEquals(0, PolygonDetection.angle(pointB, pointA));
        Assert.assertEquals(179, PolygonDetection.angle(pointA, pointB));

        pointA = new Point(527, 52, null);
        pointB = new Point(527, 36, null);

        Assert.assertEquals(90, PolygonDetection.angle(pointB, pointA));
        Assert.assertEquals(90, PolygonDetection.angle(pointA, pointB));

    }

    private boolean isSignChanged(int a, int b) {
        return !(a == b && (a ^ b) >= 0);
    }
}
