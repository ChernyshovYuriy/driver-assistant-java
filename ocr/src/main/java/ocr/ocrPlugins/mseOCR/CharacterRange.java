/**
 * CharacterRange.java
 * Copyright (c) 2003-2010 Ronald B. Cemer
 * All rights reserved.
 * This software is released under the BSD license.
 * Please see the accompanying LICENSE.txt for details.
 */
package ocr.ocrPlugins.mseOCR;

/**
 * Class to represent a range of character codes.
 *
 * @author Ronald B. Cemer
 */
public class CharacterRange {

    /**
     * The minimum character value in this range.
     */
    private final int minValue;

    /**
     * The maximum character value in this range.
     */
    private final int maxValue;

    /**
     * Construct a new {@link ocr.ocrPlugins.mseOCR.CharacterRange} object for a range of
     * character codes.
     *
     * @param min The minimum character value in this range.
     * @param max The maximum character value in this range.
     */
    public CharacterRange(final int min, final int max) {
        if (min > max) {
            throw new IllegalArgumentException("Max value must be >= Min value");
        }
        minValue = min;
        maxValue = max;
    }

    /**
     * Construct a new {@link ocr.ocrPlugins.mseOCR.CharacterRange} object for a single
     * character code.
     *
     * @param characterRange The character code for this range. This code will be both
     *                       the minimum and maximum for this range.
     */
    public CharacterRange(final int characterRange) {
        this(characterRange, characterRange);
    }

    /**
     * Return minimum character value in this range.
     *
     * @return The minimum character value in this range
     */
    public final int getMinValue() {
        return minValue;
    }

    /**
     * Return maximum character value in this range.
     *
     * @return The maximum character value in this range.
     */
    public final int getMaxValue() {
        return maxValue;
    }
}
