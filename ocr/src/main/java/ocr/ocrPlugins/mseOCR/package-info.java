/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 19.10.14
 * Time: 0:07
 */

/**
 * This package contains classes which are responsible for the 'mseOCR'.
 */
package ocr.ocrPlugins.mseOCR;
