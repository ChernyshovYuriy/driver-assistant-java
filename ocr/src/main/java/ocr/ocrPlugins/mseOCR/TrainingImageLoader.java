/**
 * TrainingImageLoader.java
 * Copyright (c) 2003-2010 Ronald B. Cemer
 * Modified by William Whitney
 * All rights reserved.
 * This software is released under the BSD license.
 * Please see the accompanying LICENSE.txt for details.
 */
package ocr.ocrPlugins.mseOCR;

import ocr.scanner.DocumentScanner;
import ocr.scanner.DocumentScannerListenerAdaptor;
import ocr.scanner.PixelImage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Utility class to load an image file, break it into individual characters,
 * and create and store multiple TrainingImage objects from the characters.
 * The source image must contain a range of characters specified in the
 * character range passed into the load() method.
 *
 * @author Ronald B. Cemer
 */
public class TrainingImageLoader extends DocumentScannerListenerAdaptor {

    /**
     * Character value.
     */
    private int charValue = 0;

    /**
     * Map of the characters and {@link ocr.ocrPlugins.mseOCR.TrainingImage}'s list.
     */
    private HashMap<Character, ArrayList<TrainingImage>> destinationCharactersMap;

    /**
     * Instance of the {@link ocr.scanner.DocumentScanner}.
     */
    private DocumentScanner documentScanner = new DocumentScanner();

    /**
     * @return The <code>DocumentScanner</code> instance that is used to load the training
     * images. This is useful if the caller wants to adjust some of the scanner's parameters.
     */
    public final DocumentScanner getDocumentScanner() {
        return documentScanner;
    }

    /**
     * Load an image containing training characters, break it up into
     * characters, and build a training set.
     * Each entry in the training set (a <code>Map</code>) has a key which
     * is a <code>Character</code> object whose value is the character code.
     * Each corresponding value in the <code>Map</code> is an
     * <code>ArrayList</code> of one or more <code>TrainingImage</code>
     * objects which contain images of the character represented in the key.
     *
     * @param pixelImage Instance of the {@link ocr.scanner.PixelImage}
     * @param charRange  A <code>CharacterRange</code> object representing the
     *                   range of characters which is contained in this image.
     * @param destinationCharacters       A <code>Map</code> which gets loaded with the training
     *                   data. Multiple calls to this method may be made with the same
     *                   <code>Map</code> to populate it with the data from several training
     *                   images.
     * @throws java.io.IOException
     */
    public final void load(
            final PixelImage pixelImage,
            final CharacterRange charRange,
            final HashMap<Character, ArrayList<TrainingImage>> destinationCharacters)
            throws IOException {

        pixelImage.toGrayScale(true);
        pixelImage.filter();
        charValue = charRange.getMinValue();
        destinationCharactersMap = destinationCharacters;
        documentScanner.scan(pixelImage, this, 0, 0, 0, 0);
        if (charValue != (charRange.getMaxValue() + 1)) {
            throw new IOException(
                    "Expected to decode " + ((charRange.getMaxValue() + 1) - charRange.getMinValue())
                            + " characters but actually decoded " + (charValue - charRange.getMinValue()));
        }
    }

    @Override
    public final void processChar(final PixelImage pixelImage,
                                  final int x1, final int y1, final int x2, final int y2,
                                  final int rowY1, final int rowY2) {

        /*Logger.d(
                "TrainingImageLoader.processChar: \'"
                        + (char) charValue + "\' " + x1 + "," + y1 + "-" + x2 + "," + y2);*/
        final int w = x2 - x1;
        final int h = y2 - y1;
        final int[] pixels = new int[w * h];
        for (int y = y1, destY = 0; y < y2; y++, destY++) {
            System.arraycopy(pixelImage.getPixels(), (y * pixelImage.getWidth()) + x1, pixels, destY * w, w);
        }
        /*for (int y = 0, idx = 0; y < h; y++) {
            for (int x = 0; x < w; x++, idx++) {
                Logger.d("" + ((pixels[idx] > 0) ? ' ' : '*'));
            }
            Logger.d("");
        }
        */
        final Character chr = new Character((char) charValue);
        ArrayList<TrainingImage> al = destinationCharactersMap.get(chr);
        if (al == null) {
            al = new ArrayList<>();
            destinationCharactersMap.put(chr, al);
        }
        al.add(new TrainingImage(pixels, w, h, y1 - rowY1, rowY2 - y2));
        charValue++;
    }
}
