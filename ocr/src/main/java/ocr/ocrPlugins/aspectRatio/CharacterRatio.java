/**
 * CharacterRatio.java
 * Copyright (c) 2010 William Whitney
 * All rights reserved.
 * This software is released under the BSD license.
 * Please see the accompanying LICENSE.txt for details.
 */
package ocr.ocrPlugins.aspectRatio;

/**
 * Provides a way to store information about a character and its ratio.
 *
 * @author William Whitney
 */
public class CharacterRatio implements Comparable<CharacterRatio> {

    /**
     * Character.
     */
    private char characterValue;

    /**
     * Ratio.
     */
    private double ratioValue;

    /**
     * Constructor.
     *
     * @param character Character
     * @param ratio     Ratio of the character
     */
    public CharacterRatio(final char character, final double ratio) {
        characterValue = character;
        ratioValue = ratio;
    }

    /**
     * Return current character value.
     * @return Character
     */
    public final char getCharacter() {
        return characterValue;
    }

    /**
     * Return ratio of the current character.
     * @return Ratio
     */
    public final double getRatio() {
        return ratioValue;
    }

    /**
     * Compares to characters and set the one with a highest ratio in the head of the List.
     * @param other {@link ocr.ocrPlugins.aspectRatio.CharacterRatio} to compare with
     * @return -1 if other character has highest ratio, 1 - otherwise and 0 if they are equals
     */
    @Override
    public final int compareTo(final CharacterRatio other) {
        if (ratioValue < other.getRatio()) {
            return -1;
        } else if (ratioValue == other.getRatio()) {
            return 0;
        } else {
            return 1;
        }
    }
}
