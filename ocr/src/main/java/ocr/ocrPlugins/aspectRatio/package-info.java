/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 18.10.14
 * Time: 19:33
 */

/**
 * This package contains classes which are responsible for the storing characters according to their ratio.
 */
package ocr.ocrPlugins.aspectRatio;
