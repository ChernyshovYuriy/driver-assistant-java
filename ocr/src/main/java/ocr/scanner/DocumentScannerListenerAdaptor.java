/**
 * DocumentScannerListenerAdapter.java
 * Copyright (c) 2003-2010 Ronald B. Cemer
 * All rights reserved.
 * This software is released under the BSD license.
 * Please see the accompanying LICENSE.txt for details.
 */
package ocr.scanner;

/**
 * Empty implementation of DocumentScannerListener interface which can be
 * subclassed and only have the needed methods overridden.  This prevents
 * implementing classes from being forced to implement all methods in the
 * interface.
 *
 * @author Ronald B. Cemer
 */
public class DocumentScannerListenerAdaptor implements DocumentScannerListener {

    /**
     * @param pixelImage The <code>PixelImage</code> containing the document
     */
    public void beginDocument(final PixelImage pixelImage) {

    }

    /**
     * @param pixelImage The <code>PixelImage</code> containing the document
     *                   which is being scanned.
     * @param y1         The y position of the top pixel row of the row of text that
     *                   is being scanned.
     * @param y2         The y position of the pixel row immediately below the last
     *                   pixel row of the row of text that is being scanned.  This is always
     *                   one more than the last pixel row of the row of text that is being
     */
    public void beginRow(final PixelImage pixelImage, final int y1, final int y2) {

    }

    /**
     * @param pixelImage The <code>PixelImage</code> containing the document
     *                   which is being scanned.
     * @param x1         The x position of the first pixel column of the character
     *                   or group of characters being scanned.
     * @param y1         The y position of the top pixel row of the row of text that
     *                   is being scanned.
     * @param x2         The x position of the pixel column immediately to the right
     *                   of the last pixel column of the character or group of characters being
     *                   scanned.  This is always one more than the last pixel column of the
     *                   character or group of characters that is being scanned.
     * @param y2         The y position of the pixel row immediately below the last
     *                   pixel row of the row of text that is being scanned.  This is always
     *                   one more than the last pixel row of the row of text that is being
     *                   scanned.
     * @param rowY1      The y position of the top scan line of the row.
     * @param rowY2      The y position of the scan line immediately below the bottom of the row.
     */
    public void processChar(final PixelImage pixelImage, final int x1, final int y1, final int x2, final int y2,
                            final int rowY1, final int rowY2) {

    }

    /**
     * @param pixelImage The <code>PixelImage</code> containing the document
     *                   which is being scanned.
     * @param x1         The x position of the first pixel column of the character
     *                   or group of characters being scanned.
     * @param y1         The y position of the top pixel row of the row of text that
     *                   is being scanned.
     * @param x2         The x position of the pixel column immediately to the right
     *                   of the last pixel column of the character or group of characters being
     *                   scanned.  This is always one more than the last pixel column of the
     *                   character or group of characters that is being scanned.
     * @param y2         The y position of the pixel row immediately below the last
     *                   pixel row of the row of text that is being scanned.  This is always
     *                   one more than the last pixel row of the row of text that is being
     */
    public void processSpace(final PixelImage pixelImage, final int x1, final int y1, final int x2, final int y2) {

    }

    /**
     * @param pixelImage The <code>PixelImage</code> containing the document
     *                   which is being scanned.
     * @param y1         The y position of the top pixel row of the row of text that
     *                   is being scanned.
     * @param y2         The y position of the pixel row immediately below the last
     *                   pixel row of the row of text that is being scanned.  This is always
     *                   one more than the last pixel row of the row of text that is being
     */
    public void endRow(final PixelImage pixelImage, final int y1, final int y2) {

    }

    /**
     * @param pixelImage The <code>PixelImage</code> containing the document
     */
    public void endDocument(final PixelImage pixelImage) {

    }
}
