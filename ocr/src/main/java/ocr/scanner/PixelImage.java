/**
 * PixelImage.java
 * Copyright (c) 2003-2010 Ronald B. Cemer
 * All rights reserved.
 * This software is released under the BSD license.
 * Please see the accompanying LICENSE.txt for details.
 */
package ocr.scanner;

import ocr.utils.Const;

/**
 * Class to contain a pixel representation of an image.
 *
 * @author Ronald B. Cemer
 */
public class PixelImage {

    /**
     * An array of pixels. This can be in RGBA or grayscale.
     * By default, it is RGBA, but if the <code>toGrayScale()</code> method
     * has been called, each pixel will be in the range of 0-255 grayscale.
     */
    protected final int[] pixels;

    /**
     * Width of the image, in pixels.
     */
    protected final int width;

    /**
     * 10-tap, lowpass Finite Impulse Response (FIR) filter.
     */
    private static final float[] FILTER_FIR_COEFFS =
            {
                    0.05001757311983922f,
                    -0.06430830829693616f,
                    -0.0900316316157106f,
                    0.1500527193595177f,
                    0.45015815807855303f,
                    0.45015815807855303f,
                    0.1500527193595177f,
                    -0.0900316316157106f,
                    -0.06430830829693616f,
                    0.05001757311983922f,
            };

    /**
     * Height of the image, in pixels.
     */
    private final int height;

    /**
     * Total number of pixels in the image (<code>width*height</code>).
     */
    private final int npix;

    /**
     * Aspect ratio of the image (<code>width/height</code>).
     */
    private final float aspectRatio;

    /**
     * Construct a new {@link ocr.scanner.PixelImage} object from an array of pixelsArray.
     *
     * @param pixelsArray An array of pixelsArray.  This can be in RGBA or grayscale.
     *               By default, it is RGBA, but if the <code>toGrayScale()</code> method
     *               has been called, each pixel will be in the range of 0-255 grayscale.
     * @param imageWidth  Width of the image, in pixelsArray.
     * @param imageHeight Height of the image, in pixelsArray.
     */
    public PixelImage(final int[] pixelsArray, final int imageWidth, final int imageHeight) {
        if (pixelsArray == null) {
            throw new IllegalArgumentException(getClass().getSimpleName() + " Constructor -> pixelsArray are null");
        }

        pixels = pixelsArray;
        width = imageWidth;
        height = imageHeight;
        npix = imageWidth * imageHeight;
        aspectRatio = ((float) imageWidth) / ((float) imageHeight);
    }

    /**
     * This method return array of the pixels.
     *
     * @return Array of the pixels
     */
    public final int[] getPixels() {
        return pixels;
    }

    /**
     * This method return width of the image.
     *
     * @return Width of the image
     */
    public final int getWidth() {
        return width;
    }

    /**
     * This method return height of the image.
     *
     * @return Height of the image
     */
    public final int getHeight() {
        return height;
    }

    /**
     * This method return aspect ratio of the image.
     *
     * @return Aspect ratio of the image
     */
    public final float getAspectRatio() {
        return aspectRatio;
    }

    /**
     * Get the index of a pixel at a specific <code>x,y</code> position.
     *
     * @param x The pixel's x position.
     * @param y The pixel's y position.
     * @return The pixel index (the index into the <code>pixels</code> array)
     * of the pixel.
     */
    public final int getPixelIndex(final int x, final int y) {
        return (y * width) + x;
    }

    /**
     * Get the value of a pixel at a specific <code>x,y</code> position.
     *
     * @param x The pixel's x position.
     * @param y The pixel's y position.
     * @return The value of the pixel.
     */
    public final int getPixel(final int x, final int y) {
        return pixels[(y * width) + x];
    }

    /**
     * // TODO : Should be a method of the Utility.
     *
     * Convert RGB pixel to gray-scale
     *
     * @param pixel Pixels' value
     * @return Gray-scale value
     */
    private static int rgbToGrayScale(final int pixel) {
        final int r = (pixel >> Const.Color.RED_SHIFT_BYTES) & Const.Color.COLOR_MASK;
        final int g = (pixel >> Const.Color.GREEN_SHIFT_BYTES) & Const.Color.COLOR_MASK;
        final int b = pixel & Const.Color.COLOR_MASK;
        final int factor = 10;
        int grayScalePixel = (r * Const.Color.RGB_2_GRAY_RED_FACTOR
                + g * Const.Color.RGB_2_GRAY_GREEN_FACTOR + b * Const.Color.RGB_2_GRAY_BLUE_FACTOR) >> factor;
        if (grayScalePixel < 0) {
            grayScalePixel = 0;
        } else if (grayScalePixel > Const.Color.MAX_COMPONENT_VALUE) {
            grayScalePixel = Const.Color.MAX_COMPONENT_VALUE;
        }
        return grayScalePixel;
    }

    /**
     * // TODO : Should be a method of the Utility.
     *
     * Convert gray-scale image to RGB
     *
     * @param pixelsArray Array of the gray-scale images' pixelsArray
     * @return RGB array of the images' pixelsArray
     */
    private int[] grayScaleToRGB(final int[] pixelsArray) {
        final int[] newPixels = new int[pixelsArray.length];
        for (int i = 0; i < newPixels.length; i++) {
            int pix = pixelsArray[i] & Const.Color.COLOR_MASK;
            newPixels[i] = pix | (pix << Const.Color.GREEN_SHIFT_BYTES)
                    | (pix << Const.Color.RED_SHIFT_BYTES) | Const.Color.BLACK_COLOR_OPAQUE;
        }
        return newPixels;
    }

    /**
     * Convert all pixels to grayscale from RGB or RGBA.
     * Do not call this method if the pixels are not currently RGB or RGBA.
     *
     * @param normalize <code>true</code> to normalize the image after converting to
     *                  grayscale, such that the darkest pixel in the image is all black and the lightest
     *                  pixel in the image is all white.
     */
    public final void toGrayScale(final boolean normalize) {
        if (npix == 0) {
            return;
        }
        if (!normalize) {
            for (int i = 0; i < npix; i++) {
                pixels[i] = rgbToGrayScale(pixels[i]);
            }
        } else {
            int pix = rgbToGrayScale(pixels[0]);
            pixels[0] = pix;
            int min = pix;
            int max = pix;
            for (int i = 1; i < npix; i++) {
                pix = rgbToGrayScale(pixels[i]);
                pixels[i] = pix;
                min = Math.min(min, pix);
                max = Math.max(max, pix);
            }
            final int range = max - min;
            if (range < 1) {
                for (int i = 0; i < npix; i++) {
                    pixels[i] = Const.Color.MAX_COMPONENT_VALUE;
                }
            } else {
                for (int i = 0; i < npix; i++) {
                    pixels[i] = Math.min(Const.Color.MAX_COMPONENT_VALUE,
                            Math.max(0, ((pixels[i] - min) * Const.Color.MAX_COMPONENT_VALUE) / range));
                }
            }
        }
    }

    /**
     * Apply filter to the image.
     */
    public final void filter() {
        filter(pixels, width, height);
    }

    /**
     * Apply filter to the image.
     *
     * @param pixelsArray Array of the images' pixels
     * @param imageWidth  Width of the image
     * @param imageHeight Height of the image
     */
    public final void filter(final int[] pixelsArray, final int imageWidth, final int imageHeight) {
        final float[] firSamples = new float[FILTER_FIR_COEFFS.length];
        float c;
        int lastPos = firSamples.length - 1;
        // Filter horizontally.
        for (int y = 0; y < imageHeight; y++) {
            for (int i = 0; i < firSamples.length; i++) {
                firSamples[i] = Const.Color.MAX_COMPONENT_FLOAT_VALUE;
            }
            int outX = -(firSamples.length >> 1);
            for (int x = 0; x < imageWidth; x++, outX++) {
                c = 0.0f;
                for (int j = 0; j < lastPos; j++) {
                    c += (firSamples[j] * FILTER_FIR_COEFFS[j]);
                    firSamples[j] = firSamples[j + 1];
                }
                c += (firSamples[lastPos] * FILTER_FIR_COEFFS[lastPos]);
                firSamples[lastPos] = getPixel(x, y);
                if (c < 0.0f) {
                    c = 0.0f;
                } else if (c > Const.Color.MAX_COMPONENT_FLOAT_VALUE) {
                    c = Const.Color.MAX_COMPONENT_FLOAT_VALUE;
                }
                if (outX >= 0) {
                    pixelsArray[getPixelIndex(outX, y)] = (int) c;
                }
            }
            while (outX < imageWidth) {
                c = 0.0f;
                for (int j = 0; j < lastPos; j++) {
                    c += (firSamples[j] * FILTER_FIR_COEFFS[j]);
                    firSamples[j] = firSamples[j + 1];
                }
                c += (firSamples[lastPos] * FILTER_FIR_COEFFS[lastPos]);
                firSamples[lastPos] = Const.Color.MAX_COMPONENT_FLOAT_VALUE;
                if (c < 0.0f) {
                    c = 0.0f;
                } else if (c > Const.Color.MAX_COMPONENT_FLOAT_VALUE) {
                    c = Const.Color.MAX_COMPONENT_FLOAT_VALUE;
                }
                pixelsArray[getPixelIndex(outX, y)] = (int) c;
                outX++;
            }
        }
        // Filter vertically.
        for (int x = 0; x < imageWidth; x++) {
            for (int i = 0; i < firSamples.length; i++) {
                firSamples[i] = Const.Color.MAX_COMPONENT_FLOAT_VALUE;
            }
            int outY = -(firSamples.length >> 1);
            for (int y = 0; y < imageHeight; y++, outY++) {
                c = 0.0f;
                for (int j = 0; j < lastPos; j++) {
                    c += (firSamples[j] * FILTER_FIR_COEFFS[j]);
                    firSamples[j] = firSamples[j + 1];
                }
                c += (firSamples[lastPos] * FILTER_FIR_COEFFS[lastPos]);
                firSamples[lastPos] = getPixel(x, y);
                if (c < 0.0f) {
                    c = 0.0f;
                } else if (c > Const.Color.MAX_COMPONENT_FLOAT_VALUE) {
                    c = Const.Color.MAX_COMPONENT_FLOAT_VALUE;
                }
                if (outY >= 0) {
                    pixelsArray[getPixelIndex(x, outY)] = (int) c;
                }
            }
            while (outY < imageHeight) {
                c = 0.0f;
                for (int j = 0; j < lastPos; j++) {
                    c += (firSamples[j] * FILTER_FIR_COEFFS[j]);
                    firSamples[j] = firSamples[j + 1];
                }
                c += (firSamples[lastPos] * FILTER_FIR_COEFFS[lastPos]);
                firSamples[lastPos] = Const.Color.MAX_COMPONENT_FLOAT_VALUE;
                if (c < 0.0f) {
                    c = 0.0f;
                } else if (c > Const.Color.MAX_COMPONENT_FLOAT_VALUE) {
                    c = Const.Color.MAX_COMPONENT_FLOAT_VALUE;
                }
                pixelsArray[getPixelIndex(x, outY)] = (int) c;
                outY++;
            }
        }
    }
}
