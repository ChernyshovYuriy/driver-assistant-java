package ocr.utils;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 19.10.14
 * Time: 22:30
 */

/**
 * Helper class which keeps library constants.
 */
public class Const {

    /**
     * Helper class which keeps colors constants.
     */
    public static class Color {

        /**
         * Maximum value of the color component.
         */
        public static final int MAX_COMPONENT_VALUE = 255;

        /**
         * Maximum value of the color component.
         */
        public static final float MAX_COMPONENT_FLOAT_VALUE = 255.0f;

        /**
         * Number of the bytes to shift to get Red value of the RGB color.
         */
        public static final int RED_SHIFT_BYTES = 16;

        /**
         * Number of the bytes to shift to get Green value of the RGB color.
         */
        public static final int GREEN_SHIFT_BYTES = 8;

        /**
         * Number of the bytes to shift to get Blue value of the RGB color.
         */
        public static final int BLUE_SHIFT_BYTES = 4;

        /**
         * Color bytes mask
         */
        public static final int COLOR_MASK = 0xFF;

        /**
         * Opaque black color
         */
        public static final int BLACK_COLOR_OPAQUE = 0xFF000000;

        /**
         * Red color factor of the RGB to Gray-scale converter formula.
         */
        public static final int RGB_2_GRAY_RED_FACTOR = 306;

        /**
         * Green color factor of the RGB to Gray-scale converter formula.
         */
        public static final int RGB_2_GRAY_GREEN_FACTOR = 601;

        /**
         * Blue color factor of the RGB to Gray-scale converter formula.
         */
        public static final int RGB_2_GRAY_BLUE_FACTOR = 117;
    }
}