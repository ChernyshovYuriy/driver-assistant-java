package ocr.ocrPlugins.scanner;

import ocr.scanner.PixelImage;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 19.10.14
 * Time: 17:11
 */
public class PixelImageTest {

    public static final int WIDTH = 7813;
    public static final int HEIGHT = 4865;

    /**
     * @return Test instance of the {@link ocr.scanner.PixelImage}
     */
    public static PixelImage getPixelImage() {
        return new PixelImage(new int[100], WIDTH , HEIGHT);
    }
}