package ocr.ocrPlugins.aspectRatio;

import ocr.ocrPlugins.mseOCR.TrainingImage;
import ocr.ocrPlugins.scanner.PixelImageTest;
import ocr.scanner.PixelImage;
import ocr.scanner.accuracy.AccuracyListener;
import ocr.scanner.accuracy.OCRIdentification;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 18.10.14
 * Time: 20:15
 */
public class AspectRatioOCRTest {

    private static final int WIDTH = 7813;
    private static final int HEIGHT = 4865;
    private AspectRatioOCR aspectRatioOCR;

    @Before
    public void setUp() throws Exception {

    }

    @Test(expected = IllegalArgumentException.class)
    public void instantWithNullMap() {
        new AspectRatioOCR(null);
    }

    @Test
    public void instantWithMap() {
        final HashMap<Character, ArrayList<TrainingImage>> map = new HashMap<>();
        aspectRatioOCR = new AspectRatioOCR(map);

        assertNotNull(aspectRatioOCR);
    }

    @Test
    public void charactersListIsEmptyIfMapIsEmpty() {
        final HashMap<Character, ArrayList<TrainingImage>> map = new HashMap<>();
        aspectRatioOCR = new AspectRatioOCR(map);

        assertNotNull(aspectRatioOCR);
        assertNull(aspectRatioOCR.getCharacterRatioAt(0));
    }

    @Test
    public void charactersListContainsCorrectElement() {
        final char character = "W".charAt(0);
        final double ratio = (double) WIDTH / (double) HEIGHT;
        final TrainingImage trainingImage = new TrainingImage(new int[100], WIDTH, HEIGHT, 0, 0);
        final ArrayList<TrainingImage> trainingImages = new ArrayList<>();
        trainingImages.add(trainingImage);
        final HashMap<Character, ArrayList<TrainingImage>> map = new HashMap<>();
        map.put(character, trainingImages);
        aspectRatioOCR = new AspectRatioOCR(map);

        assertNotNull(aspectRatioOCR);

        final CharacterRatio characterRatio = aspectRatioOCR.getCharacterRatioAt(0);

        assertNotNull(characterRatio);
        assertEquals(character, characterRatio.getCharacter());
        assertEquals(ratio, characterRatio.getRatio(), 0);
    }

    @Test
    public void listenerCallEndRaw() {
        final AccuracyListener listener = mock(AccuracyListener.class);
        final HashMap<Character, ArrayList<TrainingImage>> map = new HashMap<>();
        aspectRatioOCR = new AspectRatioOCR(map);
        aspectRatioOCR.acceptAccuracyListener(listener);

        aspectRatioOCR.endRow(PixelImageTest.getPixelImage(), PixelImageTest.WIDTH, PixelImageTest.HEIGHT);

        verify(listener, times(1)).processCharOrSpace(any(OCRIdentification.class));
    }

    @Test
    public void listenerCallProcessChar() {
        final AccuracyListener listener = mock(AccuracyListener.class);
        final HashMap<Character, ArrayList<TrainingImage>> map = new HashMap<>();
        aspectRatioOCR = new AspectRatioOCR(map);
        aspectRatioOCR.acceptAccuracyListener(listener);

        aspectRatioOCR.processChar(PixelImageTest.getPixelImage(), 0, 0, 0, 0, 0, 0);

        verify(listener, times(1)).processCharOrSpace(any(OCRIdentification.class));
    }

    @Test
    public void listenerCallProcessSpace() {
        final AccuracyListener listener = mock(AccuracyListener.class);
        final HashMap<Character, ArrayList<TrainingImage>> map = new HashMap<>();
        aspectRatioOCR = new AspectRatioOCR(map);
        aspectRatioOCR.acceptAccuracyListener(listener);

        aspectRatioOCR.processSpace(PixelImageTest.getPixelImage(), 0, 0, 0, 0);

        verify(listener, times(1)).processCharOrSpace(any(OCRIdentification.class));
    }
}
