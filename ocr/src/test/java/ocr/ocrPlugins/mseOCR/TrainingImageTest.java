package ocr.ocrPlugins.mseOCR;

import org.junit.Test;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 19.10.14
 * Time: 17:28
 */
public class TrainingImageTest {

    @Test(expected = IllegalArgumentException.class)
    public void instanceWithNullPixelsThrowException() {
        new TrainingImage(null, 0, 0, 0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void calcMSEWithNullPixelsThrowException() {
        final TrainingImage trainingImage = new TrainingImage(new int[100], 0, 0, 0, 0);
        trainingImage.calcMSE(null, 0, 0, 0, 0, 0, 0);
    }
}