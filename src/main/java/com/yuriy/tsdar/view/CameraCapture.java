package com.yuriy.tsdar.view;

import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.cpp.opencv_core;
import com.googlecode.javacv.cpp.opencv_objdetect;
import com.yuriy.tsdar.process.ImageProcessingCallback;
import com.yuriy.tsdar.process.ImageProcessingImpl;
import com.yuriy.detection.DetectedShape;
import com.yuriy.tsdar.utils.AppLogger;
import com.yuriy.tsdar.utils.AppUtils;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 3/9/14
 * Time: 6:15 PM
 */

/**
 * This class provide a functionality to capture camera stream
 */
public class CameraCapture extends Panel implements ActionListener {

    public ImagePanel imagePanel = null;
    private Timer timer;
    private FrameGrabber frameGrabber;
    private ImageProcessingImpl imageProcessing;

    /**
     * Constructor
     */
    public CameraCapture() {
        setLayout(new BorderLayout());
        setSize(AppUtils.MAIN_PANEL_WIDTH, AppUtils.MAIN_PANEL_HEIGHT);
        imagePanel = new ImagePanel();
        add(imagePanel, BorderLayout.NORTH);

        imageProcessing = new ImageProcessingImpl();
        timer = new Timer();
    }

    /**
     * Start com.yuriy.tsdar.process camera data
     *
     * @throws FrameGrabber.Exception
     */
    public void start() throws FrameGrabber.Exception {

        Loader.load(opencv_objdetect.class);

        frameGrabber = FrameGrabber.createDefault(0);
        frameGrabber.setImageHeight(AppUtils.CAMERA_HEIGHT);
        frameGrabber.setImageWidth(AppUtils.CAMERA_WIDTH);
        frameGrabber.start();

        final opencv_core.IplImage[] grabbedImage = {null};

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                try {
                    grabbedImage[0] = frameGrabber.grab();
                } catch (FrameGrabber.Exception e) {
                    AppLogger.printError(CameraCapture.class.getSimpleName() + " grab error:" + e.getMessage());
                }
                if (grabbedImage[0] != null) {
                    try {
                        imageProcessing.setCallback(new ImageProcessingCallback() {
                            @Override
                            public void onComplete(BufferedImage resultImage, final String ... args) throws IOException {
                                imagePanel.setImage(resultImage);
                            }

                            @Override
                            public void onComplete(byte[] resultImage) {

                            }

                            @Override
                            public void onDetect(DetectedShape detectedShape) {

                            }

                            @Override
                            public void onSourceImageSetup(BufferedImage sourceImage) {

                            }
                        });
                        imageProcessing.read(grabbedImage[0].getBufferedImage());
                    } catch (IOException e) {
                        AppLogger.printError(CameraCapture.class.getSimpleName() + " com.yuriy.tsdar.process error:" + e.getMessage());
                    }
                    //imagePanel.setImage(grabbedImage.getBufferedImage());
                }
            }
        }, 0, 100);
    }

    /**
     * Stop com.yuriy.tsdar.process camera data
     *
     * @throws FrameGrabber.Exception
     */
    public void stop() throws FrameGrabber.Exception {
        if (timer != null) {
            timer.cancel();
        }
        if (frameGrabber != null) {
            frameGrabber.stop();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    //Panel for Displaying Captured Image
    private class ImagePanel extends Panel {

        public Image image = null;
        private byte[] bytes;

        public ImagePanel() {
            setLayout(null);
            setSize(AppUtils.CAMERA_WIDTH, AppUtils.CAMERA_HEIGHT);
        }

        public void setImage(byte[] img) {
            bytes = img;
            repaint();
        }

        public void setImage(Image img) {
            this.image = img;
            repaint();
        }

        public void paint(Graphics g) {
            if (image != null) {
                g.drawImage(image, 0, 0, this);
            }
            /*if (bytes != null) {
                g.drawBytes(bytes, 0, bytes.length, 0, 0);
            }*/
        }
    }
}