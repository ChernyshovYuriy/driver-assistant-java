package com.yuriy.tsdar.images;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 18.10.14
 * Time: 16:57
 */

/**
 * This is a helper class which is used for loading images from the "resources" folder
 */
public final class ResourcesImagesLoader {

    /**
     * Private constructor
     */
    private ResourcesImagesLoader() {

    }


}