package com.yuriy.tsdar.process;

import com.yuriy.detection.ImageProcessingCallbackBase;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:54 AM
 */
public interface ImageProcessingCallback extends ImageProcessingCallbackBase {

    /**
     * This function is call when processing of the image is complete
     * @param resultImage {@link java.awt.image.BufferedImage} as result
     * @throws java.io.IOException
     */
    void onComplete(final BufferedImage resultImage, final String ... args) throws IOException;

    /**
     * This function is call when processing of the image is complete
     * @param resultImage array of the bytes as result
     */
    void onComplete(final byte[] resultImage);

    /**
     * This function is call when source image has been set to com.yuriy.tsdar.process
     * @param sourceImage source image
     */
    void onSourceImageSetup(BufferedImage sourceImage);
}