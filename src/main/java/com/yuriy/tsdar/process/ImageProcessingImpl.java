package com.yuriy.tsdar.process;

import com.yuriy.detection.image.ImageDataStructure;
import com.yuriy.detection.image.ImageVertex;
import com.yuriy.tsdar.utils.AppLogger;
import com.yuriy.tsdar.utils.AppUtils;
import com.yuriy.tsdar.utils.FileUtilities;
import com.yuriy.tsdar.utils.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/16/13
 * Time: 9:09 PM
 */
public final class ImageProcessingImpl implements ImageProcessing {

    private ImageProcessingCallback callback;

    /**
     * Constructor.
     */
    public ImageProcessingImpl() {
        super();
    }

    @Override
    public void read(BufferedImage sourceImage) throws IOException {

        int imageWidth = sourceImage.getWidth();
        int imageHeight = sourceImage.getHeight();
        // Scale large images
        if (imageWidth > AppUtils.SOURCE_IMG_WIDTH_LIMIT && imageHeight > AppUtils.SOURCE_IMG_HEIGHT_LIMIT) {
            sourceImage = ImageUtils.resizeImage(
                    sourceImage,
                    AppUtils.SOURCE_IMG_WIDTH_LIMIT,
                    AppUtils.SOURCE_IMG_HEIGHT_LIMIT,
                    BufferedImage.TYPE_INT_RGB
            );
        }

        imageWidth = sourceImage.getWidth();
        imageHeight = sourceImage.getHeight();

        if (callback != null) {
            callback.onSourceImageSetup(sourceImage);
        }

        final int[] pixelsCopy = sourceImage.getRGB(
                0, 0, imageWidth, imageHeight,
                null, 0, imageWidth
        ).clone();

        final Runnable redRunnable = new ImageProcessingRed(
                imageWidth, imageHeight, pixelsCopy, callback
        );
        final Runnable whiteRunnable = new ImageProcessingWhite(
                imageWidth, imageHeight, pixelsCopy, callback
        );

        new Thread(redRunnable, "RedRunnableThread").start();
        //new Thread(whiteRunnable, "WhiteRunnableThread").start();
    }

    @Override
    public void read(final String sourceImagePath) throws Exception {
        if (sourceImagePath == null) {
            AppLogger.printError(getClass().getSimpleName() + " read() method, Source Image URL must not be null");
            throw new NullPointerException();
        }
        if (sourceImagePath.equals("")) {
            AppLogger.printError(getClass().getSimpleName() + " read() method, Source Image URL must not be empty");
            throw new Exception();
        }

        read(FileUtilities.loadImage(sourceImagePath));
    }

    @Override
    public void read(final InputStream inputStream) throws Exception {
        if (inputStream == null) {
            AppLogger.printError(getClass().getSimpleName() + " read() method, Source Image stream must not be null");
            throw new NullPointerException();
        }
        read(FileUtilities.loadImage(inputStream));
    }

    public void setCallback(final ImageProcessingCallback callback) {
        this.callback = callback;
    }

    private double getMotionVector(int pointA_X, int pointA_Y, int pointB_X, int pointB_Y) {
        double a = Math.abs(pointA_Y - pointB_Y);
        double b = Math.abs(pointA_X - pointB_X);
        if (b == 0) {
            b = 1;
        }
        if (a == 0) {
            a = 1;
        }
        double d = (b / a);
        //AppLogger.printMessage("a:" + a + " b:" + b + " d:" + d);
        return Math.toDegrees(Math.atan(d));
    }

    /**
     * http://ostermiller.org/dilate_and_erode.html
     *
     * O(n) solution to find the distance to "on" pixels in a single dimension array
     *
     * @param imageGraph {@link ImageDataStructure}
     */
    private void distance(ImageDataStructure imageGraph) {
        // traverse forwards
        int pixel;
        for (int i = 0; i < imageGraph.getVertexesSize(); i++) {
            pixel = imageGraph.getVertexAt(i).getNhsRed();
            if (pixel == 255) {
                // first pass and pixel was on, it gets a zero
                //arr[i] = 0;
                imageGraph.getVertexAt(i).setNhsRed(0);
            } else {
                // pixel was off
                // It is at most the length of array
                // away from a pixel that is on

                imageGraph.getVertexAt(i).setNhsRed(imageGraph.getVertexesSize());

                // One more than the pixel to the left
                if (i > 0) {
                    imageGraph.getVertexAt(i).setNhsRed(Math.min(imageGraph.getVertexAt(i).getNhsRed(),
                                                        imageGraph.getVertexAt(i - 1).getNhsRed() + 255));
                }
            }
        }
        // traverse backwards
        for (int i = imageGraph.getVertexesSize() - 1; i >= 0; i--) {
            // what we had on the first pass
            // or one more than the pixel to the right
            if (i + 255 < imageGraph.getVertexesSize()) {
                imageGraph.getVertexAt(i).setNhsRed(Math.min(imageGraph.getVertexAt(i).getNhsRed(),
                                                    imageGraph.getVertexAt(i + 1).getNhsRed() + 255));
            }
        }
    }

    private void drawDifferences(ImageDataStructure graph, BufferedImage imageDifference) {
        //AppLogger.printMessage("Start draw differences ...");

        ImageVertex vertex;
        int pixel;
        for (int i = 0; i < graph.getVertexesSize(); i++) {
            vertex = graph.getVertexAt(i);
            pixel = vertex.getNhsRed();
            // for ARGB
            //pixel = (0xff << 24) + (pixel << 16) + (pixel << 8) + pixel;
            //pixel = (pixel << 16) + (pixel << 8) + pixel;

            if (pixel == 255) {
                //AppLogger.printMessage("pixel:" + pixel);
                pixel = 0xffffff;
            } else {
                pixel = 0;
            }

            imageDifference.setRGB(vertex.getXPosition(), vertex.getYPosition(), pixel);
        }

        //AppLogger.printMessage("Draw differences complete");
    }
}