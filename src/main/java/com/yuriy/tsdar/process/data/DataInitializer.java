package com.yuriy.tsdar.process.data;

import com.yuriy.detection.utils.TabularDictionary;
import com.yuriy.tsdar.utils.AppLogger;
import com.yuriy.tsdar.utils.ImageUtils;
import ocr.ocrPlugins.mseOCR.CharacterRange;
import ocr.ocrPlugins.mseOCR.OCRScanner;
import ocr.ocrPlugins.mseOCR.TrainingImage;
import ocr.ocrPlugins.mseOCR.TrainingImageLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 4/9/14
 * Time: 2:53 PM
 */

/**
 * This class is designed in order to handle all necessary data which is needed to process images, such as
 * SURF Dta, OCR Scanner.
 * Some of the data must be initialized prior to use in a real time application, for instance OCR Sanner must be trained,
 * SURF Data for the etalon signs must be computed in advance, some mathematics tables must be filled in advance, etc ...
 */
public final class DataInitializer {

    /**
     * Interface to provide {@link DataInitializer} lifecycle events.
     */
    public interface IDataInitializer {

        /**
         * Call when initialization complete.
         * @param dataInitializer Reference to the Data Initializer object.
         *
         * @throws Exception
         */
        void onComplete(final DataInitializer dataInitializer) throws Exception;

        /**
         * Call when an error occurs while initializing.
         * @param message Error message.
         */
        void onError(final String message);
    }

    /**
     * Width and Height of the base image which is used to interpolate with detected sign
     */
    public static final int BASE_IMAGE_WIDTH = 50;
    public static final int BASE_IMAGE_HEIGHT = 50;

    /**
     * OCR Scanner instance to use across application.
     */
    private final OCRScanner ocrScanner = new OCRScanner();

    /**
     * Reference to the Data Initializer lifecycle callback implementation.
     */
    private final IDataInitializer callback;

    /**
     * Main constructor.
     *
     * @param callback Callback reference, must not be {@code null}.
     */
    public DataInitializer(final IDataInitializer callback) {
        super();
        if (callback == null) {
            throw new NullPointerException(DataInitializer.class.getSimpleName() + " callback must specified" +
                    " before initializing");
        }
        this.callback = callback;
    }

    public final void init() throws Exception {

        // Initialize tabular data.
        TabularDictionary.init();

        // Train OCRScanner.
        //trainingOCR();

        // Invoke event and return reference to the Data Initializer.
        callback.onComplete(this);
    }

    /**
     * Returns reference to the OCR Scanner.
     *
     * @return OCR Scanner.
     */
    public final OCRScanner getOcrScanner() {
        return ocrScanner;
    }

    /**
     * Load demo training images.
     */
    private void trainingOCR() {

        ocrScanner.clearTrainingImages();
        final TrainingImageLoader trainingImageLoader = new TrainingImageLoader();
        final HashMap<Character, ArrayList<TrainingImage>> trainingImageMap = new HashMap<>();
        try {
            /**
             * STOP
             */
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("letter_s_v1.jpg"),
                    new CharacterRange('S'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("letter_s_v2.jpg"),
                    new CharacterRange('S'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("letter_t_v1.jpg"),
                    new CharacterRange('T'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("letter_o_v1.jpg"),
                    new CharacterRange('O'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("letter_p_v1.jpg"),
                    new CharacterRange('P'), trainingImageMap);
            /**
             * Digits
             */
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v1.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v2.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v3.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v4.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v5.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v6.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v7.jpg"),
                    new CharacterRange('0'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_1_v1.jpg"),
                    new CharacterRange('1'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_1_v2.jpg"),
                    new CharacterRange('1'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_2_v1.jpg"),
                    new CharacterRange('2'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_2_v2.jpg"),
                    new CharacterRange('2'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v1.jpg"),
                    new CharacterRange('3'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v2.jpg"),
                    new CharacterRange('3'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v3.jpg"),
                    new CharacterRange('3'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v4.jpg"),
                    new CharacterRange('3'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v5.jpg"),
                    new CharacterRange('3'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_4_v1.jpg"),
                    new CharacterRange('4'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_4_v2.jpg"),
                    new CharacterRange('4'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_4_v3.jpg"),
                    new CharacterRange('4'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_5_v1.jpg"),
                    new CharacterRange('5'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_5_v2.jpg"),
                    new CharacterRange('5'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_5_v3.jpg"),
                    new CharacterRange('5'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_5_v4.jpg"),
                    new CharacterRange('5'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_6_v1.jpg"),
                    new CharacterRange('6'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_6_v2.jpg"),
                    new CharacterRange('6'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_6_v3.jpg"),
                    new CharacterRange('6'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_7_v1.jpg"),
                    new CharacterRange('7'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_7_v2.jpg"),
                    new CharacterRange('7'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_8_v1.jpg"),
                    new CharacterRange('8'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_9_v1.jpg"),
                    new CharacterRange('9'), trainingImageMap);

            /*ocrScanner.acceptAccuracyListener(new AccuracyListener() {
                @Override
                public void processCharOrSpace(OCRIdentification ocrIdentification) {
                    //AppLogger.printMessage("Listener:" + ocrIdentification.toString());
                }
            });*/

            ocrScanner.addTrainingImages(trainingImageMap);
            AppLogger.printMessage("OCRScanner trained.");
        } catch (IOException ex) {
            callback.onError(DataInitializer.class.getSimpleName() + " OCR training error:" +
                    ex.getMessage());
        }
    }
}