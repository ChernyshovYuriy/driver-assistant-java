/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.tsdar.process.detection;

import com.yuriy.detection.DetectedListener;
import com.yuriy.detection.DetectionCallback;
import com.yuriy.detection.DetectionResult;
import com.yuriy.detection.RoadSign;
import com.yuriy.tsdar.utils.FileUtilities;
import com.yuriy.tsdar.utils.ImageUtils;

import java.awt.image.BufferedImage;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/10/14
 * Time: 7:34 AM
 */

/**
 * This class response for the detection of the speed value of the detected speed limit sign.
 */
public final class SpeedDetectionNA implements DetectedListener {

    private static final int GRAY_THRESHOLD = 155;

    private static final int BYTE_MASK = 0xFF;

    /**
     *
     */
    public SpeedDetectionNA() {
        super();
    }

    @Override
    public final void detectShape(final int[] detectedRed,
                                  final int[] detectedRGB,
                                  final int detectedWidth,
                                  final int detectedHeight,
                                  final DetectionCallback callback) {
        int searchR, searchG, searchB, i = -1;
        for (final int detectedPixel : detectedRed) {
            i++;

            searchR = (detectedPixel >> 16) & BYTE_MASK; //red
            if (searchR > GRAY_THRESHOLD) {
                continue;
            }
            searchG = (detectedPixel >> 8) & BYTE_MASK;  //green
            if (searchG > GRAY_THRESHOLD) {
                continue;
            }
            searchB = detectedPixel & BYTE_MASK;         //blue
            if (searchB > GRAY_THRESHOLD) {
                continue;
            }

            detectedRed[i] = 0;
        }

        final BufferedImage image = ImageUtils.intArrayToImage(
                detectedRed, detectedWidth, detectedHeight, BufferedImage.TYPE_INT_RGB
        );
//        FileUtilities.deleteImage("output", "detected_white_rect.jpg");

//        for (InterestPoint point : poi) {
//            surfDataHandler.drawPosition(point, 2, Color.RED, image);
//        }
        FileUtilities.saveImageToFile(image, "output", "detected_white_rect_" + System.currentTimeMillis() + ".jpg");

        // INFO: Skip actual detection now, focus on SURF handler

        final DetectionResult detectionResult = new DetectionResult(detectedRed, detectedWidth, detectedHeight);

        callback.onComplete(RoadSign.SPEED_LIMIT_EU, detectionResult);
    }
}
