package com.yuriy.tsdar.process;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:49 AM
 */
public interface ImageProcessing {

    void read(BufferedImage sourceImage) throws IOException;

    void read(String sourceImagePath) throws Exception;

    void read(InputStream inputStream) throws Exception;
}