package com.yuriy.tsdar.process;

import com.yuriy.detection.CompositeEllipseLikeDetector;
import com.yuriy.detection.DetectedListener;
import com.yuriy.detection.DetectedShape;
import com.yuriy.detection.DetectionCallback;
import com.yuriy.detection.matching_squares.Path;
import com.yuriy.tsdar.process.data.DataInitializer;
import com.yuriy.tsdar.process.detection.SpeedDetectionEU;
import com.yuriy.tsdar.process.detection.SpeedDetectionNA;
import com.yuriy.tsdar.process.detection.StopDetection;
import com.yuriy.tsdar.utils.AppLogger;
import com.yuriy.tsdar.utils.FileUtilities;
import com.yuriy.tsdar.utils.ImageUtils;
import ocr.ocrPlugins.mseOCR.OCRScanner;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:56 AM
 */
public class ImageProcessingCallbackImpl implements ImageProcessingCallback {

    private final OCRScanner ocrScanner;
    private int subImagesCounter;
    private BufferedImage sourceImage;

    public ImageProcessingCallbackImpl(final OCRScanner ocrScanner) {
        super();
        this.ocrScanner = ocrScanner;
        FileUtilities.deleteDifferenceImage();
    }

    @Override
    public final void onSourceImageSetup(final BufferedImage sourceImage) {

        FileUtilities.deleteSubImages();

        this.sourceImage = sourceImage;
        subImagesCounter = 0;
    }

    @Override
    public void onComplete(final BufferedImage resultImage, final String ... args) throws IOException {
        AppLogger.printMessage("Processing complete, image:" + resultImage);
        if (resultImage != null) {

            FileUtilities.createDirIfNeeded("output");

            String nameExt = "";
            if (args != null && args.length > 0) {
                nameExt = args[0];
            }
            File outputFile = new File("output/image_difference_" + nameExt + ".png");
            ImageIO.write(resultImage, "png", outputFile);
        } else {
            AppLogger.printMessage("There are no differences between images");
        }
    }

    @Override
    public void onComplete(final byte[] resultImage) {

    }

    @Override
    public void onDetect(final DetectedShape detectedShape) {
        AppLogger.printMessage(detectedShape.getShape() + " detected");

        final Path path = detectedShape.getPath();
        BufferedImage subImage = sourceImage.getSubimage(
                path.getMinX(), path.getMinY(),
                (path.getMaxX() - path.getMinX()),
                (path.getMaxY() - path.getMinY())
        );

        FileUtilities.createDirIfNeeded("output");

        subImage = ImageUtils.resizeImage(
                subImage, DataInitializer.BASE_IMAGE_WIDTH,
                DataInitializer.BASE_IMAGE_HEIGHT, BufferedImage.TYPE_BYTE_GRAY
        );

        DetectedListener speedEuRecognition = null;
        DetectedListener speedNaRecognition = null;
        DetectedListener stopRecognition = null;
        DetectedListener stopRecognition2 = null;

        if (detectedShape.getShape() == DetectedShape.SHAPE.ELLIPSE) {
            speedEuRecognition = new SpeedDetectionEU();
        }
        if (detectedShape.getShape() == DetectedShape.SHAPE.RECTANGLE) {
            speedNaRecognition = new SpeedDetectionNA();
        }
        if (detectedShape.getShape() == DetectedShape.SHAPE.OCTAGON) {
            stopRecognition = new StopDetection();
        }

        final DetectedListener compositeSpeedDetector = new CompositeEllipseLikeDetector(
                speedEuRecognition, stopRecognition, speedNaRecognition, stopRecognition2
        );

        final int[] recognizedData = new int[subImage.getWidth() * subImage.getHeight()];
        subImage.getRGB(
                0, 0, subImage.getWidth(), subImage.getHeight(), recognizedData, 0, subImage.getWidth()
        );

        compositeSpeedDetector.detectShape(
                recognizedData,
                // TODO: Implement
                new int[0],
                subImage.getWidth(),
                subImage.getHeight(),
                recognitionCallback
        );

        final File outputFile = new File("output/sub_image_" + (subImagesCounter++) + ".jpg");
        try {
            ImageIO.write(subImage, "jpg", outputFile);
        } catch (IOException e) {
            AppLogger.printError("Can not draw sub image: " + e.getMessage());
        }
    }

    private final static DetectionCallback recognitionCallback
            = (roadSign, detectionResult)
            -> AppLogger.printMessage("Recognized sign:" + roadSign + ", result:" + detectionResult);
}
