package com.yuriy.tsdar.process;

import com.yuriy.detection.CompositeShapeDetector;
import com.yuriy.detection.image.ImageDataStructure;
import com.yuriy.detection.matching_squares.MarchingSquares;
import com.yuriy.detection.matching_squares.Path;
import com.yuriy.detection.matching_squares.Point;
import com.yuriy.detection.shape.BaseDetection;
import com.yuriy.detection.shape.PolygonDetection;
import com.yuriy.detection.shape.ShapeDetectorListener;
import com.yuriy.detection.utils.PointsFactory;
import com.yuriy.detection.utils.RGBConverter;
import com.yuriy.tsdar.utils.AppLogger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Chernyshov Yurii
 * At Intellij IDEA
 * On 31/12/16
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class ImageProcessingWhite implements Runnable {

    private final ImageDataStructure imageDataStructure;
    private final MarchingSquares marchingSquares = new MarchingSquares();
    private final java.util.List<Path> paths = new ArrayList<>();
    private final int imageWidth;
    private final int imageHeight;
    private final int[] rgbPixels;
    private final ImageProcessingCallback callback;

    ImageProcessingWhite(final int imageWidth, final int imageHeight, final int[] rgbPixels,
                         final ImageProcessingCallback callback) {
        super();
        imageDataStructure = new ImageDataStructure();
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.rgbPixels = rgbPixels;
        this.callback = callback;
    }

    @Override
    public void run() {
        if (!imageDataStructure.isInit()) {
            imageDataStructure.init(imageWidth * imageHeight);
        }

        final BufferedImage imageDifference = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);

        imageDataStructure.reset();

        RGBConverter.toBinaryWhite(
                rgbPixels,
                imageDataStructure
        );

        final int[] binaryData = imageDataStructure.getWhiteAsArray();

        int counter = 0;
        for (final int pixel : binaryData) {
            if (pixel >= 1) {
                binaryData[counter] = Color.RED.getRGB();
            } else {
                binaryData[counter] = 0;
            }
            counter++;
        }

        final BufferedImage imageTest = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        imageTest.setRGB(0, 0, imageWidth, imageHeight, binaryData, 0 , imageWidth);
        File outputFile = new File("output/white_rect_preprocess.png");
        try {
            ImageIO.write(imageTest, "png", outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final long start = System.currentTimeMillis();
        marchingSquares.init(imageWidth, imageHeight, binaryData);
        paths.clear();
        marchingSquares.identifyPerimeter(paths);
        AppLogger.printMessage("Time to get Paths:" + (System.currentTimeMillis() - start) + " ms");

        Path path;

        // Eliminate small objects
        int minWidth = imageWidth * 6 / 100;
        int minHeight = imageHeight * 6 / 100;
        for (int j = 0; j < paths.size(); ++j) {
            path = paths.get(j);
            if (path.getShapeWidth() < minWidth || path.getShapeHeight() < minHeight) {
                paths.remove(path);
                j--;
            }
        }

        // Eliminate nested shapes
//        Path path_I;
//        for (int j = 0; j < paths.size(); ++j) {
//
//            path = paths.get(j);
//            for (int i = j + 1; i < paths.size(); ++i) {
//
//                path_I = paths.get(i);
//                if (path.getMinX() <= path_I.getMinX() && path.getMinY() <= path_I.getMinY() &&
//                        path.getMaxX() >= path_I.getMaxX() && path.getMaxY() >= path_I.getMaxY()) {
//
//                    paths.remove(path_I);
//                    i--;
//                }
//            }
//        }

//        for (final Path pathToScan : paths) {
//            final GrahamScan grahamScan = new GrahamScan(points);
//            points = grahamScan.hull();
//        }

        final BaseDetection rectDetection = new PolygonDetection();
        final ShapeDetectorListener compositeRecognizer = new CompositeShapeDetector(
                rectDetection
        );

        for (final Path pathFinal : paths) {
            final Point[] points = PointsFactory.createPoints(
                    pathFinal.getOriginX(), pathFinal.getOriginY(), pathFinal.getDirections()
            );
            for (final Point point : points) {
                if (point.x < imageWidth && point.y < imageHeight) {
                    imageDifference.setRGB(point.x, point.y, Color.RED.getRGB());
                }
            }

            // TODO : Implement data transferring to detector.
            final int[] dataRed = new int[0];
            final int[] dataRGB = new int[0];
            compositeRecognizer.detectShape(pathFinal, dataRed, dataRGB, callback::onDetect);
        }

        try {
            callback.onComplete(imageDifference, "white");
        } catch (final IOException e) {
            AppLogger.printError("On complete with exception:" + e.getMessage());
        }
    }
}
