package com.yuriy.tsdar.utils;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 21.10.14
 * Time: 21:16
 */
public final class AppUtils {

    private AppUtils() {
        super();
    }

    /**
     * Dimensions
     */
    public static final int MAIN_PANEL_WIDTH = 640;
    public static final int MAIN_PANEL_HEIGHT = 640;
    public static final int CAMERA_WIDTH = 640;
    public static final int CAMERA_HEIGHT = 480;
    public static final int SOURCE_IMG_WIDTH_LIMIT = 640;
    public static final int SOURCE_IMG_HEIGHT_LIMIT = 480;
}