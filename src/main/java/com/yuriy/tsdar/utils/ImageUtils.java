package com.yuriy.tsdar.utils;

import com.yuriy.tsdar.images.ResourcesImagesLoader;
import ocr.scanner.PixelImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/13/14
 * Time: 9:41 PM
 */
public final class ImageUtils {

    /**
     * Private constructor
     */
    private ImageUtils() {
        super();
    }

    /**
     * Resize an image to provided dimensions
     *
     * @param originalImage  original image
     * @param resizeToWidth  width value resize to
     * @param resizeToHeight height value to resize to
     * @param type           type of the image
     * @return scaled image
     */
    public static BufferedImage resizeImage(final BufferedImage originalImage,
                                            int resizeToWidth,
                                            int resizeToHeight,
                                            final int type) {
        final double originalWidth = originalImage.getWidth();
        final double originalHeight = originalImage.getHeight();
        final double widthRatio = originalWidth / originalHeight;
        final double heightRatio = originalHeight / originalWidth;
        if (originalWidth >= originalHeight) {
            resizeToHeight = (int) (resizeToWidth * heightRatio);
        } else {
            resizeToWidth = (int) (resizeToHeight * widthRatio);
        }

        //AppLogger.printMessage("Resize image to w:" + resizeToWidth + " h:" + resizeToHeight +
        //        " wRatio:" + widthRatio + " hRatio:" + heightRatio);

        final BufferedImage resizedImage = new BufferedImage(resizeToWidth, resizeToHeight, type);
        final Graphics2D graphics = resizedImage.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics.drawImage(originalImage, 0, 0, resizeToWidth, resizeToHeight, null);
        graphics.dispose();
        return resizedImage;
    }

    /**
     * Load an Image from the resources and transform it into the {@link ocr.scanner.PixelImage}.
     *
     * @param imagePath Path to the image file in the resources
     * @return {@link ocr.scanner.PixelImage}
     * @throws IOException
     */
    public static PixelImage resourceImageToPixelImage(final String imagePath) throws IOException {
        final InputStream inputStream = ResourcesImagesLoader.class.getResourceAsStream(imagePath);
        final BufferedImage bufferedImage = ImageIO.read(inputStream);
        final int[] pixelsArray = new int[bufferedImage.getWidth() * bufferedImage.getHeight()];
        bufferedImage.getRGB(
                0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), pixelsArray,
                0, bufferedImage.getWidth()
        );
        return new PixelImage(pixelsArray, bufferedImage.getWidth(), bufferedImage.getHeight());
    }

    /**
     *
     * @param rgbImage
     * @return
     */
    public static BufferedImage rgbToGrayScale(final BufferedImage rgbImage) {
        final BufferedImage imageOrigGray = new BufferedImage(
                rgbImage.getWidth(),
                rgbImage.getHeight(),
                BufferedImage.TYPE_BYTE_GRAY);
        final Graphics graphics = imageOrigGray.getGraphics();
        graphics.drawImage(rgbImage, 0, 0, null);
        graphics.dispose();
        return imageOrigGray;
    }

    /**
     *
     * @param data
     * @param width
     * @param height
     * @param type
     * @return
     */
    public static BufferedImage intArrayToImage(final int[] data, final int width, final int height, final int type) {
        final BufferedImage image = new BufferedImage(width, height, type);
        image.setRGB(0, 0, width, height, data, 0, width);
        return image;
    }
}