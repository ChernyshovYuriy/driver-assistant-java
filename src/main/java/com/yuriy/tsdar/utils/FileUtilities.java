package com.yuriy.tsdar.utils;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/16/13
 * Time: 9:09 PM
 */

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class contains help methods to com.yuriy.tsdar.process File
 */
public final class FileUtilities {

    private FileUtilities() {
        super();
    }

    /**
     * Load Image from file.
     *
     * @param fileName Name of the file.
     * @return {@link java.awt.image.BufferedImage}.
     * @throws java.io.IOException
     */
    public static BufferedImage loadImage(final String fileName) throws IOException {
        return ImageIO.read(new File(fileName));
    }

    /**
     * Load Image from input stream.
     *
     * @param inputStream {@link java.io.InputStream} of the image file.
     * @return {@link java.awt.image.BufferedImage}.
     * @throws IOException
     */
    public static BufferedImage loadImage(final InputStream inputStream) throws IOException {
        return ImageIO.read(inputStream);
    }

    /**
     * This method creates a directory with given name is such does not exists.
     *
     * @param path a path to the directory.
     */
    public static void createDirIfNeeded(String path) {
        final File file = new File(path);
        if (file.exists() && !file.isDirectory()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static void deleteImage(final String dir, final String name) {
        final File outputFile = new File(dir, name);
        if (outputFile.exists()) {
            outputFile.delete();
        }
    }

    public static void deleteDifferenceImage() {
        deleteImage("output", "image_difference.png");
    }

    public static void deleteSubImages() {
        final File outputDir = new File("output");
        if (!outputDir.isDirectory()) {
            return;
        }

        final FilenameFilter subImageFilter = (dir, name) -> {
            final String lowercaseName = name.toLowerCase();
            return lowercaseName.startsWith("sub_image");
        };

        final File[] files = outputDir.listFiles(subImageFilter);
        for (final File outputFile : files) {
            if (outputFile.exists()) {
                outputFile.delete();
            }
        }
    }

    public static void clearOutputDir() {
        final File outputDir = new File("output");
        if (!outputDir.isDirectory()) {
            return;
        }
        final File[] files = outputDir.listFiles();
        for (final File outputFile : files) {
            if (outputFile.exists()) {
                outputFile.delete();
            }
        }
        //AppLogger.printMessage(
        //        result ? "Output dir deleted" : "Output dir not deleted"
        //);
    }

    /**
     *
     * @param data
     * @param width
     * @param height
     * @param type
     */
    public static void saveIntArrayToFile(final int[] data, final int width, final int height, final int type,
                                          final String dir, final String fileName) {
        final BufferedImage image = new BufferedImage(width, height, type);
        image.setRGB(0, 0, width, height, data, 0, width);
        saveImageToFile(image, dir, fileName);
    }

    /**
     *
     * @param image
     * @param dir
     * @param fileName
     */
    public static void saveImageToFile(final BufferedImage image, final String dir, final String fileName) {
        final File outputFile = new File(dir, fileName);
        try {
            ImageIO.write(image, "jpg", outputFile);
        } catch (final IOException e) {
            AppLogger.printError("Can not save image to file:" + e.getMessage());
        }
    }
}