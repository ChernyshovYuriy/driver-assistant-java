package com.yuriy.tsdar;

import com.yuriy.tsdar.images.ResourcesImagesLoader;
import com.yuriy.tsdar.process.ImageProcessingCallback;
import com.yuriy.tsdar.process.ImageProcessingCallbackImpl;
import com.yuriy.tsdar.process.ImageProcessingImpl;
import com.yuriy.tsdar.process.data.DataInitializer;
import com.yuriy.tsdar.utils.AppLogger;
import com.yuriy.tsdar.utils.FileUtilities;

import java.lang.ref.WeakReference;

/**
 * Main class, point entry to the project.
 */
public final class Main {

    /**
     * Point entry.
     *
     * @param args Array of arguments, may be empty.
     */
    public static void main(final String[] args) {
        AppLogger.printMessage("Traffic Signs Detection and Recognition");

        new Main();
    }

    /**
     * Main constructor.
     */
    private Main() {
        FileUtilities.clearOutputDir();
        // Initialize data needed for the project.
        try {
            initializeData();
        } catch (final Exception e) {
            AppLogger.printError("Init data error:" + e.getMessage());
        }

//        processCamera();
    }

    /**
     * Initialize data needed for the project.
     * All the necessary initialization must be done within {@link DataInitializer}.
     *
     * @throws Exception
     */
    private void initializeData() throws Exception {
        final DataInitializer dataInitializer = new DataInitializer(
                new DataInitializerListener(this)
        );
        dataInitializer.init();
    }

    /**
     * Entry point to actual processing. All initialization required must be done at this point.
     *
     * @param dataInitializer Reference to the Dat Initializer object.
     * @throws Exception
     */
    private void doProcess(final DataInitializer dataInitializer) throws Exception {
        // Define image name to process.
        final String sourceImagePath = "20170305_073304.jpg";

        // Create callback class instance for the image processor.
        final ImageProcessingCallback imageProcessingCallback = new ImageProcessingCallbackImpl(
                dataInitializer.getOcrScanner()
        );

        // Initialize image processor and do actual processing.
        final ImageProcessingImpl imageProcessing = new ImageProcessingImpl();
        imageProcessing.setCallback(imageProcessingCallback);
        // For additional robustness / test / etc ... next block can be run several times.
        for (int i = 0; i < 1; i++) {
            imageProcessing.read(ResourcesImagesLoader.class.getResourceAsStream(sourceImagePath));
        }
    }

    /**
     * Listener for the {@link DataInitializer} events, such as onComplete.
     */
    private static final class DataInitializerListener implements DataInitializer.IDataInitializer {

        /**
         * Reference to the enclosing class.
         */
        private final WeakReference<Main> reference;

        /**
         * Main constructor.
         *
         * @param reference       Reference to the enclosing class.
         */
        private DataInitializerListener(final Main reference) {
            super();
            this.reference = new WeakReference<>(reference);
        }

        @Override
        public final void onComplete(final DataInitializer dataInitializer) throws Exception {
            final Main main = reference.get();
            if (main == null) {
                AppLogger.printError("Can not complete Data Initialization, reference to enclosing class is null");
                return;
            }
            // Data initialization completed, do actual processing!
            main.doProcess(dataInitializer);
        }

        @Override
        public final void onError(final String message) {
            AppLogger.printError(message);
        }
    }

    /*private void processCamera() {

        final CameraCapture cameraCapture = new CameraCapture();
        try {
            cameraCapture.start();
        } catch (FrameGrabber.Exception e) {
            AppLogger.printError("Process camera FrameGrabber.Exception:" + e.getMessage());
        }

        Frame frame = new Frame("Camera View");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                try {
                    cameraCapture.stop();
                } catch (FrameGrabber.Exception e1) {
                    AppLogger.printError("Process camera stop:" + e1.getMessage());
                }
                System.exit(0);
            }
        });
        frame.add("Center", cameraCapture);
        frame.pack();
        frame.setSize(new Dimension(AppUtils.MAIN_PANEL_WIDTH, AppUtils.MAIN_PANEL_HEIGHT));
        frame.setVisible(true);
    }*/
}