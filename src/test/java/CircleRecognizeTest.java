import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import com.yuriy.tsdar.process.ImageProcessingCallbackImpl;
import com.yuriy.tsdar.process.ImageProcessingImpl;
import com.yuriy.detection.DetectedShape;
import com.yuriy.tsdar.utils.AppLogger;
import com.yuriy.detection.utils.TabularDictionary;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/6/14
 * Time: 8:49 PM
 */
public class CircleRecognizeTest {

    @Before
    public void setUp() throws Exception {
        TabularDictionary.init();
    }

    @Test
    public void testRecognizeCircles() throws Exception {

        final int[] counter = {0};
        final int signsNumber = 1;

        String fileName;
        ImageProcessingImpl imageProcessing = new ImageProcessingImpl();

        final CountDownLatch countDownLatch = new CountDownLatch(signsNumber);

        imageProcessing.setCallback(new ImageProcessingCallbackImpl(null) {

            @Override
            public void onDetect(DetectedShape detectedShape) {
                AppLogger.printMessage("Result: " + detectedShape.getShape());
                if (detectedShape.getShape() == DetectedShape.SHAPE.ELLIPSE) {
                    counter[0]++;
                }
            }

            @Override
            public void onComplete(BufferedImage resultImage, final String ... args) throws IOException {
                super.onComplete(resultImage);

                countDownLatch.countDown();
            }
        });

        for (int i = 1; i < signsNumber + 1; i++) {

            if (i < 10) {
                fileName = "0" + i;
            } else {
                fileName = String.valueOf(i);
            }

            AppLogger.printMessage("");

            InputStream inputStream = this.getClass().getResourceAsStream("circle_" + fileName + ".jpg");

            AppLogger.printMessage("Read image input stream:" + inputStream);

            imageProcessing.read(inputStream);
        }

        countDownLatch.await(5, TimeUnit.SECONDS);

        AppLogger.printMessage("");
        AppLogger.printMessage("Recognize " + counter[0] + " signs from " + signsNumber);

        Assert.assertEquals(signsNumber, counter[0]);
    }
}